# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'inventory'				                                # The application name
set :repo_url, 'git@bitbucket.org:bherring/inventory.git'	          # The repository to pull from
set :branch, ENV["TAG"] || 'master'					                        # The branch in the repository to deploy from
set :deploy_to, '/var/www/inventory'					                      # The directory to deploy to
set :log_level, :debug							                                # The logging level for capistrano
set :keep_releases, 5							                                  # The number of release to keep on the destination servers
set :gem_set, 'ruby-2.1.5@inventory'          				              # The RVM gemset to use for this application



######################
# DO NOT EDIT BELLOW #
######################
# Shared
set :linked_files, %w{config/database.yml config/environments/production.rb config/app_config.yml config/initializers/devise.rb}
set :linked_dirs, %w{log public/system}

# RVM
set :rvm_type, :system
set :rvm_ruby_version, fetch(:gem_set)

# Rails
set :rails_env, 'production'
set :migration_role, :primary_app
set :assets_roles, [:web, :app]

# Bundler
set :bundle_roles, :app
set :bundle_jobs, 4
set :bundle_flags, '--quiet --deployment'

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app) do
      execute :touch, current_path.join('tmp/restart.txt')
      execute '/sbin/service inventory_job restart'
    end
  end

  desc 'Create Directories'
  task :create_directories do
    on roles(:app) do
      execute "[ -d #{release_path.join('tmp')} ] || mkdir -p #{release_path.join('tmp')}"
      execute "[ -d #{release_path.join('tmp','pids')} ] || mkdir -p #{release_path.join('tmp','pids')}"

      #SpreadSheets directory
      execute "[ -d #{shared_path.join('public')} ] || mkdir -p #{shared_path.join('public')}"
      execute "[ -d #{shared_path.join('public','system')} ] || mkdir -p #{shared_path.join('public','system')}"

      execute "[ -d #{release_path.join('public')} ] || mkdir -p #{release_path.join('public')}"
    end
  end

  desc 'Upload Configurations'
  task :upload_configurations do
    on roles(:app) do
      # Create the config directory if it doesn't exist
      execute "[ -d #{shared_path.join('config')} ] || mkdir -p #{shared_path.join('config')}"
      execute "[ -d #{shared_path.join('config', 'environments')} ] || mkdir -p #{shared_path.join('config', 'environments')}"

      # Upload the configuration files
      set :stage_source, "config/deploy/config/#{fetch(:stage)}"
      set :all_source, 'config/deploy/config/all'
      upload! "#{fetch(:stage_source)}/database.yml", shared_path.join('config/database.yml')
      upload! "#{fetch(:stage_source)}/app_config.yml", shared_path.join('config/app_config.yml')
      upload! "#{fetch(:stage_source)}/environments/#{fetch(:stage)}.rb", shared_path.join("config/environments/#{fetch(:stage)}.rb")
      upload! "#{fetch(:stage_source)}/initializers/devise.rb", shared_path.join('config/initializers/devise.rb')
    end
  end

  desc 'Setup RVM'
  task :setup_rvm do
    on roles(:app) do
      # Create the RVM gemset if it doesn't already exist
      execute "cd #{shared_path.join('config')} && rvm gemset use #{fetch(:gem_set)} ; [ $? -eq 0 ] || rvm --ruby-version use #{fetch(:gem_set)} --create"
      execute "gem list --local | grep bundler > /dev/null ; [ $? -eq 1 ] || /usr/local/rvm/bin/rvm #{fetch(:gem_set)} do gem install bundler"
    end
  end

  desc 'Post Migration'
  task :post_migration do
    on roles(:app) do
    end
  end


  # Cap hooks
  before 'rvm:hook', 'deploy:setup_rvm'
  before :starting, :upload_configurations
  after :updated, 'bundler:install'
  after 'deploy:migrate', :post_migration
  after :updated, :create_directories
  after :finished, :restart
end
