if ActiveRecord::Base.connection.table_exists? 'users'
  ANONYMOUS_USER_FNAME = 'Anonymous'
  ANONYMOUS_USER_LNAME = 'User'

  User.create(:fname => ANONYMOUS_USER_FNAME, :lname => ANONYMOUS_USER_LNAME) unless User.find_by_fname_and_lname(ANONYMOUS_USER_FNAME, ANONYMOUS_USER_LNAME)

  ANONYMOUS_USER_ID = User.find_by_fname_and_lname(ANONYMOUS_USER_FNAME, ANONYMOUS_USER_LNAME).id
end