require 'yaml'

app_config_path = Rails.root.join('config', 'app_config.yml')
if File.exist? app_config_path
  APP_CONFIG = YAML.load_file(Rails.root.join('config', 'app_config.yml'))[Rails.env]
else
  raise 'Missing config/app_config.yml'
end

API_VERSIONS = [
    1,
].sort