Inventory::Application.routes.draw do
  resources :server_applications


  authenticated :user do
    root :to => 'inventory_spreadsheets#index', :as => :authenticated_root
  end

  root :to => redirect('/users/sign_in')


  devise_for :users, :controllers => { :users => 'users', :registrations => 'registrations', :confirmations => 'confirmations', :password_expired => 'password_expired', :omniauth_callbacks => 'users/omniauth_callbacks' }
  devise_scope :user do
    put '/confirm' => 'confirmations#confirm'
  end

  resource :my_account, :controller => :my_account, :only => [:show]  do
    resource :password_expired, :only => [:show, :update], :controller => :password_expired
    resources :jobs
  end

  resources :inventory_spreadsheets do
    member do
      get :new_email
      get :send_via_email
    end
    collection do
      get :generate
    end
  end

  resources :inventory_diagrams do
    member do
      get :new_email
      get :send_via_email
    end
    collection do
      get :generate
      get :show_generate
    end
  end
  resources :applications
  resources :application_versions
  resources :servers
  resources :application_family_applications
  resources :application_families
  resources :server_databases
  resources :databases
  resources :server_relationships
  resources :jobs
  resources :tenant_spaces
  resources :data_centers
  resources :environments
  resources :zones

  resources :audits

  namespace :admin do
    match '/' => 'admin#index'
    resources :users,  :controllers => { :users => 'admin/users'}
    resources :jobs
  end

  namespace :api, defaults: {:format => :json} do
    root :to => 'versions#index'

    resources :versions

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :applications
      resources :application_versions
      resources :servers
      resources :jobs
      resources :inventory_spreadsheets do
        member do
          get :send_via_email
        end
        collection do
          get :generate
        end
      end
      resources :inventory_diagrams do
        member do
          get :send_via_email
        end
        collection do
          get :generate
        end
      end
    end
  end
end