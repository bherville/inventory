require 'test_helper'

class ServerApplicationsControllerTest < ActionController::TestCase
  setup do
    @server_application = server_applications(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:server_applications)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create server_application" do
    assert_difference('ServerApplication.count') do
      post :create, server_application: { application_id: @server_application.application_id, description: @server_application.description, server_id: @server_application.server_id }
    end

    assert_redirected_to server_application_path(assigns(:server_application))
  end

  test "should show server_application" do
    get :show, id: @server_application
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @server_application
    assert_response :success
  end

  test "should update server_application" do
    put :update, id: @server_application, server_application: { application_id: @server_application.application_id, description: @server_application.description, server_id: @server_application.server_id }
    assert_redirected_to server_application_path(assigns(:server_application))
  end

  test "should destroy server_application" do
    assert_difference('ServerApplication.count', -1) do
      delete :destroy, id: @server_application
    end

    assert_redirected_to server_applications_path
  end
end
