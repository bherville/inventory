require 'test_helper'

class InventoryDiagramsControllerTest < ActionController::TestCase
  setup do
    @inventory_diagram = inventory_diagrams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inventory_diagrams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inventory_diagram" do
    assert_difference('InventoryDiagram.count') do
      post :create, inventory_diagram: { user_id: @inventory_diagram.user_id }
    end

    assert_redirected_to inventory_diagram_path(assigns(:inventory_diagram))
  end

  test "should show inventory_diagram" do
    get :show, id: @inventory_diagram
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inventory_diagram
    assert_response :success
  end

  test "should update inventory_diagram" do
    put :update, id: @inventory_diagram, inventory_diagram: { user_id: @inventory_diagram.user_id }
    assert_redirected_to inventory_diagram_path(assigns(:inventory_diagram))
  end

  test "should destroy inventory_diagram" do
    assert_difference('InventoryDiagram.count', -1) do
      delete :destroy, id: @inventory_diagram
    end

    assert_redirected_to inventory_diagrams_path
  end
end
