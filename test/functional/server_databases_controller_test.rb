require 'test_helper'

class ServerDatabasesControllerTest < ActionController::TestCase
  setup do
    @server_database = server_databases(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:server_databases)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create server_database" do
    assert_difference('ServerDatabase.count') do
      post :create, server_database: { database_id: @server_database.database_id, server_id: @server_database.server_id }
    end

    assert_redirected_to server_database_path(assigns(:server_database))
  end

  test "should show server_database" do
    get :show, id: @server_database
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @server_database
    assert_response :success
  end

  test "should update server_database" do
    put :update, id: @server_database, server_database: { database_id: @server_database.database_id, server_id: @server_database.server_id }
    assert_redirected_to server_database_path(assigns(:server_database))
  end

  test "should destroy server_database" do
    assert_difference('ServerDatabase.count', -1) do
      delete :destroy, id: @server_database
    end

    assert_redirected_to server_databases_path
  end
end
