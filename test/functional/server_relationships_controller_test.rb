require 'test_helper'

class ServerRelationshipsControllerTest < ActionController::TestCase
  setup do
    @server_relationship = server_relationships(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:server_relationships)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create server_relationship" do
    assert_difference('ServerRelationship.count') do
      post :create, server_relationship: { port: @server_relationship.port, server_primary_id: @server_relationship.server_primary_id, server_secondary_id: @server_relationship.server_secondary_id }
    end

    assert_redirected_to server_relationship_path(assigns(:server_relationship))
  end

  test "should show server_relationship" do
    get :show, id: @server_relationship
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @server_relationship
    assert_response :success
  end

  test "should update server_relationship" do
    put :update, id: @server_relationship, server_relationship: { port: @server_relationship.port, server_primary_id: @server_relationship.server_primary_id, server_secondary_id: @server_relationship.server_secondary_id }
    assert_redirected_to server_relationship_path(assigns(:server_relationship))
  end

  test "should destroy server_relationship" do
    assert_difference('ServerRelationship.count', -1) do
      delete :destroy, id: @server_relationship
    end

    assert_redirected_to server_relationships_path
  end
end
