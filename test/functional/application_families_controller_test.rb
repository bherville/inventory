require 'test_helper'

class ApplicationFamiliesControllerTest < ActionController::TestCase
  setup do
    @application_family = application_families(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:application_families)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create application_family" do
    assert_difference('ApplicationFamily.count') do
      post :create, application_family: { application_id: @application_family.application_id, last_checked_at: @application_family.last_checked_at, name: @application_family.name }
    end

    assert_redirected_to application_family_path(assigns(:application_family))
  end

  test "should show application_family" do
    get :show, id: @application_family
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @application_family
    assert_response :success
  end

  test "should update application_family" do
    put :update, id: @application_family, application_family: { application_id: @application_family.application_id, last_checked_at: @application_family.last_checked_at, name: @application_family.name }
    assert_redirected_to application_family_path(assigns(:application_family))
  end

  test "should destroy application_family" do
    assert_difference('ApplicationFamily.count', -1) do
      delete :destroy, id: @application_family
    end

    assert_redirected_to application_families_path
  end
end
