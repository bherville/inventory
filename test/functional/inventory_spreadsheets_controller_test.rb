require 'test_helper'

class InventorySpreadsheetsControllerTest < ActionController::TestCase
  setup do
    @inventory_spreadsheet = inventory_spreadsheets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:inventory_spreadsheets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create inventory_spreadsheet" do
    assert_difference('InventorySpreadsheet.count') do
      post :create, inventory_spreadsheet: { spreadsheet: @inventory_spreadsheet.spreadsheet, user_id: @inventory_spreadsheet.user_id }
    end

    assert_redirected_to inventory_spreadsheet_path(assigns(:inventory_spreadsheet))
  end

  test "should show inventory_spreadsheet" do
    get :show, id: @inventory_spreadsheet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @inventory_spreadsheet
    assert_response :success
  end

  test "should update inventory_spreadsheet" do
    put :update, id: @inventory_spreadsheet, inventory_spreadsheet: { spreadsheet: @inventory_spreadsheet.spreadsheet, user_id: @inventory_spreadsheet.user_id }
    assert_redirected_to inventory_spreadsheet_path(assigns(:inventory_spreadsheet))
  end

  test "should destroy inventory_spreadsheet" do
    assert_difference('InventorySpreadsheet.count', -1) do
      delete :destroy, id: @inventory_spreadsheet
    end

    assert_redirected_to inventory_spreadsheets_path
  end
end
