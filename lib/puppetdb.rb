require 'puppetinventory'
require 'puppetrest'

module Puppetdb
  def self.retrieve_resources(app_config, resource_context)
    nodes = get_puppet_nodes app_config


    resources = Hash.new

    nodes.each do |node, facts|
      attributes = {
        :name         => node,
        :puppet_facts => facts
      }


      resources[node] = resource_context.new(attributes)
    end

    resources

  end

  def self.retrieve_hardware_resources(app_config, resource_context)
    facts = get_puppet_fact app_config, resource_context::PUPPET_NAME, "#{resource_context.to_s.underscore}.json"

    uniq_facts = facts.map{|r| r['value']}.uniq

    resources = Hash.new

    uniq_facts.each do |value|
      attributes = {
          :name         => value
      }


      resources[value] = resource_context.new(attributes)
    end

    resources
  end


  def self.valid_app_config?(app_config)
    if APP_CONFIG['puppetdb'] && APP_CONFIG['puppetdb']['api_base_url']
      true
    else
      #TODO: Raise exception
      false
    end
  end

  private
  def self.get_puppet_nodes(app_config)
    if valid_app_config? app_config
      nodes_json = File.join(Rails.root, 'tmp', 'nodes.json')

      if File.exist? nodes_json
        file = File.read nodes_json
        nodes = JSON.parse file
      else
        puppet_inventory = Puppetinventory::Inventory.new(APP_CONFIG['puppetdb']['api_base_url'], true)
        nodes = puppet_inventory.get_nodes
      end
    end

    nodes
  end

  def self.get_puppet_fact(app_config, fact, cached_json = nil)
    if valid_app_config? app_config
      nodes_json = File.join(Rails.root, 'tmp', cached_json)

      if File.exist? nodes_json
        file = File.read nodes_json
        facts = JSON.parse file
      else
        puppet_inventory = PuppetRest.new(APP_CONFIG['puppetdb']['api_base_url'])
        facts = puppet_inventory.get_puppetdb("facts/#{fact}")
      end
    end

    facts
  end
end