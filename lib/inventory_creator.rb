module InventoryCreator
  HEADER_ROW = 0

  def self.generate(servers, app_config, server_databases, server_relationships, application_family_applications, options = {})
    if valid_app_config? app_config
      book = Spreadsheet::Workbook.new

      add_server_sheet(book, servers, app_config['puppetdb']['facts'])
      add_major_applications_sheet(book, servers)
      add_minor_applications_sheet(book, servers)
      add_application_family_applications_sheet(book, application_family_applications) if application_family_applications.count > 0
      add_application_urls_sheet(book, servers.map{ |s| s.server_applications }.flatten)
      add_server_databases_sheet(book, server_databases) if server_databases.count > 0
      add_server_relationships_sheet(book, server_relationships) if server_relationships.count > 0
      add_audits_sheet(book, [servers, ((servers.map { |s| s.server_applications }).flatten.map { |sa| sa.application_version })], options[:audits])

      book
    else
      #TODO: Raise exception
      nil
    end
  end

  private
  def self.valid_app_config?(app_config)
    if APP_CONFIG['puppetdb'] && app_config['puppetdb']['facts']
      true
    else
      #TODO: Raise exception
      false
    end
  end

  def self.add_server_sheet(book, servers, facts)
    vm_sheet = book.create_worksheet(:name => 'Target VMs')
    length = 0

    #Process the column headers
    vm_sheet[0,0]
    column = HEADER_ROW
    facts.each do |key, fact|
      vm_sheet[0,column] = fact
      length = fact.length

      column = column + 1
    end


    #Process the data rows
    row = 1
    (servers.sort_by { |s| s.name }).each do |server|
      n = server.puppet_facts
      column = 0
      facts.each do |key, fact|
        value = n[key]

        if value.is_a? String
          value = n[key].upcase
        end

        vm_sheet[row,column] = value

        if value && length < value.length
          length = value.length
        end

        column = column + 1
      end

      row = row + 1
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold

    wrap = Spreadsheet::Format.new :text_wrap => true
    every_other = Spreadsheet::Format.new :text_wrap => true



    #Set the column widths
    column_count = 0
    facts.each do |key, fact|
      vm_sheet.column(column_count).width = length + 5

      column_count = column_count + 1
    end

    #Format the column headers
    facts.count.times do |c|
      vm_sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    row = 3

    servers.count.times do |n|
      facts.count.times do |c|
        vm_sheet.row(row).set_format(c, every_other)
        vm_sheet.row(row - 1).set_format(c, wrap)
      end

      row = row + 2
    end

    return book
  end

  def self.add_major_applications_sheet(book, servers)
    sheet = book.create_worksheet(:name => 'Major Applications')
    columns = ['Name', 'Server', 'Tenant Space', 'Data Center', 'Environment', 'Zone', 'Type', 'Version', 'Sends Emails']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    (servers.sort_by { |s| s.name }).each do |s|
      if s.server_applications
        s.server_applications.each do |as|
          next unless as.application.major_application?
          column = 0

          # Application Name
          value = as.application.name
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Server Name
          value = s.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Server Tenant Space
          value = s.tenant_space.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Server Data Center
          value = s.data_center.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Server Environment
          value = s.environment.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Server Zone
          value = s.zone.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Application Type
          value = as.application.app_type
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Application Version
          if as.application_version && as.application_version.application_version
            value = as.application_version.application_version.strip
          else
            value = 'N/A'
          end
          sheet[row,column] = value
          if value && sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Sends Emails
          value = as.application.sends_emails? ? 'Yes' : 'No'
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          sheet[row,column] = value

          row = row + 1
        end
      end
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_minor_applications_sheet(book, servers)
    sheet = book.create_worksheet(:name => 'Minor Applications')
    columns = ['Name', 'Server', 'Tenant Space', 'Data Center', 'Environment', 'Zone', 'Type', 'Version', 'Sends Emails']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    (servers.sort_by { |s| s.name }).each do |s|
      if s.server_applications
        s.server_applications.each do |as|
          next if as.application.major_application?
          column = 0

          # Application Name
          value = as.application.name
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Server Name
          value = s.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length + 10
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Server Tenant Space
          value = s.tenant_space.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Server Data Center
          value = s.data_center.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Server Environment
          value = s.environment.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Server Zone
          value = s.zone.name.upcase
          sheet[row,column] = value
          if sheet.column(column).width < value.length + 5
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Application Type
          value = as.application.app_type
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Application Version
          if as.application_version && as.application_version.application_version
            value = as.application_version.application_version.strip
          else
            value = 'N/A'
          end
          sheet[row,column] = value
          if value && sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          column = column + 1

          # Sends Emails
          value = as.application.sends_emails? ? 'Yes' : 'No'
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          sheet[row,column] = value

          row = row + 1
        end
      end
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_application_urls_sheet(book, server_applications)
    sheet = book.create_worksheet(:name => 'Application URLs')
    columns = ['URL', 'Application Name', 'Server', 'Tenant Space', 'Data Center', 'Environment', 'Zone', 'Type']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    server_applications.sort_by { |sa| sa.application.name }.each do |sa|
      next unless sa.application.major_application?

      sa.urls.each do |url|
        column = 0

        # URL
        value = url
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 5
        end
        column = column + 1

        # Application Name
        value = sa.application.name
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 5
        end
        column = column + 1

        # Server Name
        value = sa.server.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
        column = column + 1

        # Server Tenant Space
        value = sa.server.tenant_space.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
        column = column + 1

        # Server Data Center
        value = sa.server.data_center.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Server Environment
        value = sa.server.environment.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Server Zone
        value = sa.server.zone.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Application Type
        value = sa.application.app_type
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 5
        end

        row = row + 1
      end
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_server_databases_sheet(book, server_databases)
    sheet = book.create_worksheet(:name => 'Databases')
    columns = ['Server Name', 'Server Data Center', 'Server Environment', 'Application Name', 'Database Name', 'DBMS']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    ((server_databases.select{ |sd| sd.server }).sort_by { |sd| sd.server.name}).each do |server_database|
      next unless server_database.server && server_database.database
      column = 0

      # Server Name
      value = server_database.server.name.upcase
      sheet[row,column] = value
      if sheet.column(column).width < value.length
        sheet.column(column).width = value.length + 10
      end
      column = column + 1

      # Server Data Center
      value = server_database.server.data_center.name.upcase
      sheet[row,column] = value
      if sheet.column(column).width < value.length
        sheet.column(column).width = value.length
      end
      column = column + 1

      # Server Environment
      value = server_database.server.environment.name.upcase
      sheet[row,column] = value
      if sheet.column(column).width < value.length
        sheet.column(column).width = value.length
      end
      column = column + 1

      # Application Name
      if server_database.database.application
        value = server_database.database.application.name
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 7
        end
      end
      column = column + 1

      # Database Name
      value = server_database.database.name
      sheet[row,column] = value
      if sheet.column(column).width < value.length
        sheet.column(column).width = value.length + 7
      end
      column = column + 1

      # DBMS
      value = server_database.database.dbms.upcase
      sheet[row,column] = value
      if sheet.column(column).width < value.length
        sheet.column(column).width = value.length + 5
      end
      sheet[row,column] = value

      row = row + 1
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_server_relationships_sheet(book, server_relationships)
    sheet = book.create_worksheet(:name => 'Server Relationships')
    columns = ['Source Server Name', 'Source Server Data Center', 'Source Server Environment', 'Source Server Zone', 'Destination Server Name', 'Destination Server Data Center', 'Destination Server Environment', 'Destination Server Zone', 'Port', 'Description']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    (server_relationships.sort_by { |sr| sr.server_primary.name}).each do |server_relationship|
      next unless server_relationship.server_primary && server_relationship.server_secondary
      column = 0

      # Primary Server Name
      if server_relationship.server_primary.name
        value = server_relationship.server_primary.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
      end
      column = column + 1

      # Primary Server Data Center
      if server_relationship.server_primary.data_center
        value = server_relationship.server_primary.data_center.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Primary Server Environment
      if server_relationship.server_primary.environment
        value = server_relationship.server_primary.environment.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Primary Server Zone
      if server_relationship.server_primary.zone
        value = server_relationship.server_primary.zone.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Secondary Server Name
      if server_relationship.server_secondary.name
        value = server_relationship.server_secondary.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
      end
      column = column + 1

      # Secondary Server Data Center
      if server_relationship.server_secondary.data_center
        value = server_relationship.server_secondary.data_center.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Secondary Server Environment
      if server_relationship.server_secondary.environment
        value = server_relationship.server_secondary.environment.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Secondary Server Zone
      if server_relationship.server_secondary.zone
        value = server_relationship.server_secondary.zone.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end
      column = column + 1

      # Port
      port = server_relationship.port.to_i
      value = (port > 0 ? port : 'N/A' )
      sheet[row,column] = value
      column = column + 1

      # Description
      if server_relationship.description
        value = server_relationship.description
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
      end

      row = row + 1
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_application_family_applications_sheet(book, application_family_applications)
    sheet = book.create_worksheet(:name => 'Application Families')
    columns = ['Application Family', 'Application Name', 'Server Name', 'Server Tenant Space', 'Server Data Center', 'Server Environment', 'Server Zone']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row
    (application_family_applications.sort_by { |afa| afa.application.name}).each do |application_family_application|
      servers = application_family_application.servers.uniq
      servers = servers.sort_by { |s| s.name }

      servers.each do |s|
        column = 0

        # Application Family Name
        value = application_family_application.application_family.name
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
        column = column + 1

        # Application Family Application Name
        value = application_family_application.application.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Server Name
        value = s.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 10
        end
        column = column + 1

        # Server Tenant Space
        value = s.tenant_space.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Server Data Center
        value = s.data_center.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        column = column + 1

        # Server Environment
        value = s.environment.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length + 5
        end
        column = column + 1

        # Server Zone
        value = s.zone.name.upcase
        sheet[row,column] = value
        if sheet.column(column).width < value.length
          sheet.column(column).width = value.length
        end
        sheet[row,column] = value

        row = row + 1
      end
    end

    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  def self.add_audits_sheet(book, resource_array, options = {})
    start_date = options ? options[:start_date] : nil
    start_date ||= 1.week.ago

    sheet = book.create_worksheet(:name => audit_sheet_name(start_date))
    columns = ['Audit Resource Name', 'Audit Resource Type' , 'Associated Resource(s)', 'Audit Version', 'Attribute', 'Original Value', 'New Value', 'Changed On']

    column_length = 0
    column_num = 0

    columns.each do |c|
      sheet[HEADER_ROW,column_num] = c

      if column_length < c.length
        sheet.column(column_num).width = c.length + 5
      end

      column_num = column_num + 1
    end

    data_start_row = 1


    row = data_start_row

    resource_array.flatten.each do |resource|
      next unless resource && resource.audits

      resource.audits.each do |audit|
        # Skip the audit if the created at date does not fall within the valid range.
        next unless audit.created_at.between?(start_date, Time.now)

        audit.audited_changes.each do |audited_change, values|
          next unless values && values.is_a?(Array)

          column = 0

          # Audit Resource Name
          value = resource.to_s
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Audit Resource Type
          value = resource.class.name.titlecase
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Audit Associated Resource
          if resource.respond_to?(:audit_association)
            value = resource.audit_association.map { |a| "#{a.class.name.titlecase}: #{a}" }.join("\n")
          else
            value = 'N/A'
          end
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Audit Version
          value = audit.version
          sheet[row,column] = value
          column = column + 1

          # Attribute
          value = audited_change
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 10
          end
          column = column + 1

          # Original Value
          value = values.first
          sheet[row,column] = value
          if value && value.is_a?(String) && sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # New Value
          value = values.last
          sheet[row,column] = value
          if value && value.is_a?(String) && sheet.column(column).width < value.length
            sheet.column(column).width = value.length
          end
          column = column + 1

          # Changed On
          value = audit.created_at.to_formatted_s(:long_ordinal)
          sheet[row,column] = value
          if sheet.column(column).width < value.length
            sheet.column(column).width = value.length + 5
          end
          sheet[row,column] = value

          row = row + 1
        end
      end
    end


    #Format
    header = Spreadsheet::Format.new  :weight => :bold
    every_other = Spreadsheet::Format.new :text_wrap => true


    #Format the column headers
    sheet.columns.count.times do |c|
      sheet.row(HEADER_ROW).set_format(c, header)
    end

    #Format the rows
    rows = sheet.rows.count

    row = data_start_row
    rows.times do
      sheet.columns.count.times do |c|
        sheet.row(row).set_format(c, every_other)
      end

      row = row + 2
    end

    return book
  end

  private
  def self.audit_sheet_name(start_date)
    days_ago = (Time.now - start_date).to_i / 1.day

    case days_ago
      when 0
        date_string = "Today's"
      when 1
        date_string = "24 Hour's"
      when 2..6
        date_string = "#{days_ago} Day's"
      when 7..13
        date_string = "Week's"
      else
        date_string = "#{(days_ago/7).ceil} Week's"
    end

    "Last #{date_string} Audits"
  end
end