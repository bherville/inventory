namespace :inventory_spreadsheet do
  desc 'Generate an inventory spreadsheet for all servers and applications'
  task :generate_all => :environment do
    attr = {
        :servers              => Server.all,
        :application_versions => ApplicationVersion.all,
        :server_databases     => ServerDatabase.all,
        :server_relationships => ServerRelationship.all
    }

    spreadsheet = InventorySpreadsheet.create_and_generate attr

    puts spreadsheet.spreadsheet.path
  end

  desc 'Generate and send an inventory spreadsheet for all servers and applications via email. Specify email address in a comma (.) delimited list.'
  task :generate_and_send_all => :environment do
    email_addresses = ENV['EMAIL_ADDRESSES']

    if email_addresses.nil?
      puts 'Please enter the comma (\') decimated list of email addresses you want to send the inventory spreadsheet to.'
      email_addresses = STDIN.gets.strip
    end


    attr = {
        :servers              => Server.all,
        :application_versions => ApplicationVersion.all,
        :server_databases     => ServerDatabase.all,
        :server_relationships => ServerRelationship.all
    }

    puts 'Queuing the generation of the inventory spreadsheet...'
    InventorySpreadsheet.create_and_generate_async attr, :xls, { :email_addresses => email_addresses }
  end

  desc 'Generate an inventory spreadsheet for using filters'
  task :generate_by_filter => :environment do
    params = Server.find_by_environment_attributes_to_params(ENV['TENANT_SPACE'], ENV['DATA_CENTER'], ENV['ENVIRONMENT'], ENV['ZONE'], ENV['APPLICATION'], ENV['APPLICATION_FAMILY'])

    servers = Server.find_by_environment_attributes(params)

    attr = {
        :servers              => servers,
        :application_versions => (servers.map { |s| s.application_versions }).flatten.uniq,
        :server_databases     => (servers.map { |s| s.server_databases }).flatten.uniq,
        :server_relationships => (servers.map { |s| s.server_relationships }).flatten.uniq,
        :filter_params        => params
    }

    spreadsheet = InventorySpreadsheet.create_and_generate attr

    puts spreadsheet.spreadsheet.url
    puts spreadsheet.spreadsheet.path
  end

  desc 'Generate and send an inventory spreadsheet using filters via email. Specify email address in a comma (.) delimited list.'
  task :generate_and_send_by_filter => :environment do
    email_addresses = ENV['EMAIL_ADDRESSES']
    params = Server.find_by_environment_attributes_to_params(ENV['TENANT_SPACE'], ENV['DATA_CENTER'], ENV['ENVIRONMENT'], ENV['ZONE'], ENV['APPLICATION'], ENV['APPLICATION_FAMILY'])

    servers = Server.find_by_environment_attributes(params)

    attr = {
        :servers              => servers,
        :application_versions => (servers.map { |s| s.application_versions }).flatten.uniq,
        :server_databases     => (servers.map { |s| s.server_databases }).flatten.uniq,
        :server_relationships => (servers.map { |s| s.server_relationships }).flatten.uniq,
        :filter_params        => params
    }

    puts 'Generating the inventory spreadsheet...'
    InventorySpreadsheet.create_and_generate_async attr, :xls, { :email_addresses => email_addresses }
  end
end
