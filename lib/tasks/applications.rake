namespace :applications do
  desc 'Sync changes with the external APIs'
  task :sync => :environment do
    Application.create_and_update
    Application.remove_stale
  end
end
