namespace :servers do
  desc 'Sync changes with the external APIs'
  task :sync => :environment do
    Server.create_and_update
    Server.remove_stale
  end
end
