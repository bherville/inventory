namespace :application_versions do
  desc 'Sync changes with the external APIs'
  task :sync => :environment do
    ApplicationVersion.create_and_update
    ApplicationVersion.remove_stale
  end
end
