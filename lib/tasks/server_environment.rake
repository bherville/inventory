namespace :server_environment do
  desc 'Sync Data Cetner changes with the external APIs'
  task :sync_data_center => :environment do
    DataCenter.create_and_update
    DataCenter.remove_stale
  end

  desc 'Sync Environment changes with the external APIs'
  task :sync_environment => :environment do
    Environment.create_and_update
    Environment.remove_stale
  end

  desc 'Sync Tenant Space changes with the external APIs'
  task :sync_tenant_space => :environment do
    TenantSpace.create_and_update
    TenantSpace.remove_stale
  end

  desc 'Sync Zone changes with the external APIs'
  task :sync_zone => :environment do
    Zone.create_and_update
    Zone.remove_stale
  end

  desc 'Sync Server changes with the external APIs'
  task :sync => :environment do
    Server.create_and_update
    Server.remove_stale
  end

  desc 'Sync Application changes with the external APIs'
  task :sync => :environment do
    Application.create_and_update
    Application.remove_stale
  end

  desc 'Sync Application Version changes with the external APIs'
  task :sync => :environment do
    ApplicationVersion.create_and_update
    ApplicationVersion.remove_stale
  end

  desc 'Sync changes with the external APIs'
  task :sync_all => :environment do
    puts 'Syncing Data Center...'
    puts "#{DataCenter.create_and_update.count.to_s} Data Center(s) Synchronized..."
    puts "#{DataCenter.remove_stale.count.to_s} Data Center(s) Deleted..."
    puts ''
    puts 'Syncing Environment...'
    puts "#{Environment.create_and_update.count.to_s} Environment(s) Synchronized..."
    puts "#{Environment.remove_stale.count.to_s} Environment(s) Deleted..."
    puts ''
    puts 'Syncing Tenant Space...'
    puts "#{TenantSpace.create_and_update.count.to_s} Tenant Space(s) Synchronized..."
    puts "#{TenantSpace.remove_stale.count.to_s} Tenant Space(s) Deleted..."
    puts ''
    puts 'Syncing Zone...'
    puts "#{Zone.create_and_update.count.to_s} Zone(s) Synchronized..."
    puts "#{Zone.remove_stale.count.to_s} Zone(s) Deleted..."
    puts ''
    puts 'Syncing Server...'
    puts "#{Server.create_and_update.count.to_s} Server(s) Synchronized..."
    puts "#{Server.remove_stale.count.to_s} Server(s) Deleted..."
    puts ''
    puts 'Syncing Application...'
    puts "#{Application.create_and_update.count.to_s} Application(s) Synchronized..."
    puts "#{Application.remove_stale.count.to_s} Application(s) Deleted..."
    puts ''
    puts 'Syncing Database...'
    puts "#{Database.create_and_update.count.to_s} Database(s) Synchronized..."
    puts "#{Database.remove_stale.count.to_s} Database(s) Deleted..."
    puts ''
    puts 'Syncing Server Relationship...'
    puts "#{ServerRelationship.create_and_update.count.to_s} Server Relationship(s) Synchronized..."
    puts "#{ServerRelationship.remove_stale.count.to_s} Server Relationship(s) Deleted..."
    puts ''
    puts 'Syncing Server Database...'
    puts "#{ServerDatabase.create_and_update.count.to_s} Server Database(s) Synchronized..."
    puts "#{ServerDatabase.remove_stale.count.to_s} Server Database(s) Deleted..."
    puts ''
    puts 'Syncing Application Family...'
    puts "#{ApplicationFamily.create_and_update.count.to_s} Application Family(ies) Synchronized..."
    puts "#{ApplicationFamily.remove_stale.count.to_s} Application Family(ies) Deleted..."
    puts ''
    puts 'Syncing Application Family Application...'
    puts "#{ApplicationFamilyApplication.create_and_update.count.to_s} Application Family Application(s) Synchronized..."
    puts "#{ApplicationFamilyApplication.remove_stale.count.to_s} Application Family Application(s) Deleted..."
    puts ''
    puts 'Syncing Server Application...'
    puts "#{ServerApplication.create_and_update.count.to_s} Server Application(s) Synchronized..."
    puts "#{ServerApplication.remove_stale.count.to_s} Server Application(s) Deleted..."
    puts ''
    puts 'Syncing Application Version...'
    puts "#{ApplicationVersion.create_and_update.count.to_s} Application Version(s) Synchronized..."
    puts "#{ApplicationVersion.remove_stale.count.to_s} Application Version(s) Deleted..."
    puts ''
  end

end
