namespace :inventory_diagram do
  desc 'Generate an inventory diagram for all servers and applications'
  task :generate_all => :environment do
    attr = {
        :servers              => Server.all
    }

    diagram = InventoryDiagram.create_and_generate attr

    puts diagram.diagram.path
  end

  desc 'Generate and send an inventory diagram for all servers and applications via email. Specify email address in a comma (.) delimited list.'
  task :generate_and_send_all => :environment do
    email_addresses = ENV['EMAIL_ADDRESSES']

    if email_addresses.nil?
      puts 'Please enter the comma (\') decimated list of email addresses you want to send the inventory diagram to.'
      email_addresses = STDIN.gets.strip
    end


    attr = {
        :servers              => Server.all
    }

    puts 'Generating the inventory diagram...'
    diagram = InventoryDiagram.create_and_generate attr

    puts 'Queuing email of inventory diagram...'
    diagram.send_email_delayed email_addresses.split(',')
  end

  desc 'Generate an inventory diagram for using filters'
  task :generate_by_filter => :environment do
    params = Server.find_by_environment_attributes_to_params(ENV['TENANT_SPACE'], ENV['DATA_CENTER'], ENV['ENVIRONMENT'], ENV['ZONE'], ENV['APPLICATION'], ENV['APPLICATION_FAMILY'])

    servers = Server.find_by_environment_attributes(params)

    attr = {
        :servers        => servers,
        :filter_params  => params
    }

    diagram = InventoryDiagram.create_and_generate attr

    puts diagram.diagram.url
    puts diagram.diagram.path
  end

  desc 'Generate and send an inventory diagram using filters via email. Specify email address in a comma (.) delimited list.'
  task :generate_and_send_by_filter => :environment do
    email_addresses = ENV['EMAIL_ADDRESSES']
    params = Server.find_by_environment_attributes_to_params(ENV['TENANT_SPACE'], ENV['DATA_CENTER'], ENV['ENVIRONMENT'], ENV['ZONE'], ENV['APPLICATION'], ENV['APPLICATION_FAMILY'])

    servers = Server.find_by_environment_attributes(params)

    attr = {
        :servers        => servers,
        :filter_params  => params
    }

    puts 'Generating the inventory diagram...'
    diagram = InventoryDiagram.create_and_generate attr

    puts 'Queuing email of inventory diagram...'
    diagram.send_email_delayed_filtered email_addresses.split(','), params
  end

  desc 'Generate and send an inventory spreadsheet using filters via email. Specify email address in a comma (.) delimited list.'
  task :generate_and_send_default_by_tenant_space => :environment do
    tenant_space = ENV['TENANT_SPACE']

    if tenant_space
      params = Server.find_by_environment_attributes_to_params(tenant_space, nil, nil, nil, nil, nil)

      servers = Server.find_by_environment_attributes(params)
      process_default_inventory_diagram ENV['EMAIL_ADDRESSES'], (servers.map { |s| s.tenant_space}).uniq
    else
      process_default_inventory_diagram ENV['EMAIL_ADDRESSES']
    end
  end

  def process_default_inventory_diagram(email_addresses = nil, tenant_spaces = TenantSpace.order(:name))
    tenant_spaces.each do |t|
      puts "Processing TenantSpace #{t.name}"
      params = Server.find_by_environment_attributes_to_params(t.name, nil, nil, nil, nil, nil)

      servers = Server.find_by_environment_attributes(params)

      data_centers = (servers.map { |s| s.data_center}).uniq

      data_centers.each do |dc|
        puts "\tProcessing DataCenter #{dc.name}"

        dc_params = Server.find_by_environment_attributes_to_params(t.name, dc.name, nil, nil, nil, nil)

        dc_servers = Server.find_by_environment_attributes(dc_params)

        dc_environments = (dc_servers.map { |s| s.environment}).uniq

        dc_environments.each do |dce|
          puts "\t\tProcessing Environment #{dce.name}"
          dce_params = Server.find_by_environment_attributes_to_params(t.name, dc.name, dce.name, nil, nil, nil)

          dce_servers = Server.find_by_environment_attributes(dce_params)

          attr = {
              :servers              => dce_servers,
              :filter_params        => dce_params
          }


          puts "\t\t\tGenerating the inventory diagram..."
          InventoryDiagram.create_and_generate_async attr, :png, { :email_addresses => email_addresses }
        end
      end
    end
  end
end
