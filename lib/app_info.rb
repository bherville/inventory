module AppInfo
  def self.api_url(app_config, end_point)
    if AppInfo.valid_app_config?(app_config) && app_config['app_info']['api_base_url']
      "#{app_config['app_info']['api_base_url']}/#{end_point}"
    else
      #TODO: Raise error message
    end
  end

  def self.retrieve_resources_json(app_config, end_point)
    if AppInfo.valid_app_config?(app_config)
      self.get_json app_config, end_point
    else
      nil
    end
  end

  def self.retrieve_resources(app_config, end_point, resource_context, resource_attributes, primary_key)
    if AppInfo.valid_app_config?(app_config)
      api_resources = self.get_json app_config, end_point

      resources = Hash.new

      api_resources.each do |api_resource|
        next unless AppInfo.include?(app_config, api_resource)

        attributes = Hash.new

        resource_attributes.each do |resource_attribute, attribute_str|
          attributes[resource_attribute] = api_resource[attribute_str]
        end


        resources[api_resource[primary_key]] = resource_context.new(attributes)
      end

      resources
    else
      nil
    end
  end

  def self.valid_app_config?(app_config)
    if app_config['app_info']
      true
    else
      #TODO: Raise exception
      false
    end
  end

  private
  def self.get_json(app_config, end_point)
    timeout = app_config['app_info']['timeout']
    timeout ||= 120

    JSON.parse(RestClient::Request.execute(:method => :get, :url => "#{self.api_url(app_config, end_point)}.json", :timeout => timeout, :open_timeout => timeout, accept: :json,  content_type: :json))
  end

  def self.include?(app_config, api_resource)
    include_inactive = false

    if AppInfo.valid_app_config?(app_config)
      if app_config['app_info']['include_inactive']
        include_inactive = app_config['appinfo']['include_inactive'] == 'true' ? true : false
      else
        include_inactive = false
      end
    end

    if include_inactive
      true
    else
      api_resource['ignore_active'] || api_resource['active']
    end
  end
end