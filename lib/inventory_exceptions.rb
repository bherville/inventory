module InventoryExceptions
  class DuplicateWorkingDirectory < StandardError
  end

  class NoGraph < StandardError
  end

  class InvalidGraph < StandardError
  end
end