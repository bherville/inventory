# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160304182511) do

  create_table "application_families", :force => true do |t|
    t.string   "name"
    t.boolean  "ignore_active",   :default => false
    t.datetime "last_checked_at", :default => '2015-05-15 20:07:43', :null => false
    t.boolean  "active",          :default => true
    t.integer  "user_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
  end

  create_table "application_family_applications", :force => true do |t|
    t.integer  "application_id"
    t.integer  "application_family_id"
    t.boolean  "ignore_active",         :default => false
    t.datetime "last_checked_at",       :default => '2015-05-15 20:07:43', :null => false
    t.boolean  "active",                :default => true
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
  end

  create_table "application_versions", :force => true do |t|
    t.text     "application_version"
    t.integer  "server_id"
    t.integer  "application_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "server_application_id"
  end

  create_table "application_versions_inventory_spreadsheets", :force => true do |t|
    t.integer "application_version_id"
    t.integer "inventory_spreadsheet_id"
  end

  create_table "applications", :force => true do |t|
    t.string   "name"
    t.string   "app_type"
    t.integer  "user_id"
    t.boolean  "sends_emails"
    t.string   "path"
    t.boolean  "major_application"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "data_centers", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "databases", :force => true do |t|
    t.string   "name"
    t.string   "dbms"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "application_id"
  end

  create_table "environments", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "inventory_diagrams", :force => true do |t|
    t.integer  "user_id"
    t.datetime "generated_at"
    t.datetime "generation_completed_at"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "diagram_file_name"
    t.string   "diagram_content_type"
    t.integer  "diagram_file_size"
    t.datetime "diagram_updated_at"
    t.string   "name"
    t.string   "diagram_key_file_name"
    t.string   "diagram_key_content_type"
    t.integer  "diagram_key_file_size"
    t.datetime "diagram_key_updated_at"
    t.text     "filter_params"
  end

  create_table "inventory_diagrams_servers", :force => true do |t|
    t.integer "server_id"
    t.integer "inventory_diagram_id"
  end

  create_table "inventory_spreadsheets", :force => true do |t|
    t.integer  "user_id"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "spreadsheet_file_name"
    t.string   "spreadsheet_content_type"
    t.integer  "spreadsheet_file_size"
    t.datetime "spreadsheet_updated_at"
    t.datetime "generated_at"
    t.datetime "generation_completed_at"
    t.text     "filter_params"
  end

  create_table "inventory_spreadsheets_server_databases", :force => true do |t|
    t.integer "server_database_id"
    t.integer "inventory_spreadsheet_id"
  end

  create_table "inventory_spreadsheets_server_relationships", :force => true do |t|
    t.integer "server_relationship_id"
    t.integer "inventory_spreadsheet_id"
  end

  create_table "inventory_spreadsheets_servers", :force => true do |t|
    t.integer "server_id"
    t.integer "inventory_spreadsheet_id"
  end

  create_table "jobs", :force => true do |t|
    t.string   "job_id"
    t.integer  "statusable_id"
    t.string   "statusable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.text     "session_id"
    t.datetime "completed_at"
    t.string   "status"
    t.integer  "user_id"
  end

  add_index "jobs", ["user_id"], :name => "index_jobs_on_user_id"

  create_table "server_applications", :force => true do |t|
    t.integer  "server_id"
    t.integer  "application_id"
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.text     "urls"
  end

  create_table "server_databases", :force => true do |t|
    t.integer  "server_id"
    t.integer  "database_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "server_relationships", :force => true do |t|
    t.integer  "server_primary_id"
    t.integer  "server_secondary_id"
    t.integer  "port"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.text     "description"
  end

  create_table "servers", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.text     "puppet_facts"
    t.integer  "data_center_id"
    t.integer  "environment_id"
    t.integer  "tenant_space_id"
    t.integer  "zone_id"
    t.integer  "processor_count"
    t.decimal  "memory_size"
    t.integer  "total_disk_space", :limit => 8
  end

  create_table "tenant_spaces", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "fname"
    t.string   "lname"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0,  :null => false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "provider"
    t.integer  "uid"
    t.string   "time_zone"
    t.integer  "roles_mask",             :default => 2
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["provider"], :name => "index_users_on_provider"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["uid"], :name => "index_users_on_uid"
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "zones", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
