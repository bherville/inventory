class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :name
      t.string :app_type
      t.integer :user_id
      t.boolean :sends_emails
      t.string :path
      t.boolean :major_application

      t.timestamps
    end
  end
end
