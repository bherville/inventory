class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_id
      t.references :statusable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
