class CreateApplicationVersions < ActiveRecord::Migration
  def change
    create_table :application_versions do |t|
      t.string :application_version
      t.integer :server_id
      t.integer :application_id

      t.timestamps
    end
  end
end
