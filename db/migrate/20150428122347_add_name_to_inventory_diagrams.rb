class AddNameToInventoryDiagrams < ActiveRecord::Migration
  def change
    add_column :inventory_diagrams, :name, :string
  end
end
