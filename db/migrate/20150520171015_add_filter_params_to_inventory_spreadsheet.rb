class AddFilterParamsToInventorySpreadsheet < ActiveRecord::Migration
  def change
    add_column :inventory_spreadsheets, :filter_params, :text
  end
end
