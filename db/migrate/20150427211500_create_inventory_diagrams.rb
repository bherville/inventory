class CreateInventoryDiagrams < ActiveRecord::Migration
  def change
    create_table :inventory_diagrams do |t|
      t.integer :user_id
      t.timestamp :generated_at
      t.timestamp :generation_completed_at

      t.timestamps
    end

    add_attachment :inventory_diagrams, :diagram
  end
end
