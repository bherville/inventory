class CreateApplicationVersionsInventorySpreadsheets < ActiveRecord::Migration
  def change
    create_table :application_versions_inventory_spreadsheets do |t|
      t.integer :application_version_id
      t.integer :inventory_spreadsheet_id
    end
  end
end
