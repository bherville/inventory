class CreateDataCenters < ActiveRecord::Migration
  def change
    create_table :data_centers do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
