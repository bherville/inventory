class AddServerApplicationIdToApplicationVersion < ActiveRecord::Migration
  def change
    add_column :application_versions, :server_application_id, :integer
  end
end
