class AddFilterParamsToInventoryDiagram < ActiveRecord::Migration
  def change
    add_column :inventory_diagrams, :filter_params, :text
  end
end
