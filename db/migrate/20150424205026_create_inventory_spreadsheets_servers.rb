class CreateInventorySpreadsheetsServers < ActiveRecord::Migration
  def change
    create_table :inventory_spreadsheets_servers do |t|
      t.integer :server_id
      t.integer :inventory_spreadsheet_id
    end
  end
end
