class AddSessionIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :session_id, :text
  end
end
