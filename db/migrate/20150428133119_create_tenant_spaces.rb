class CreateTenantSpaces < ActiveRecord::Migration
  def change
    create_table :tenant_spaces do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
