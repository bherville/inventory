class CreateApplicationFamilies < ActiveRecord::Migration
  def change
    create_table :application_families do |t|
      t.string :name
      t.boolean :ignore_active, :default => false
      t.datetime :last_checked_at, :null => false, :default => Time.now
      t.boolean :active, :default => true
      t.integer :user_id

      t.timestamps
    end
  end
end
