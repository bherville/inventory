class CreateInventoryDiagramsServers < ActiveRecord::Migration
  def change
    create_table :inventory_diagrams_servers do |t|
      t.integer :server_id
      t.integer :inventory_diagram_id
    end
  end
end
