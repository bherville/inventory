class CreateInventorySpreadsheetsServerDatabases < ActiveRecord::Migration
  def change
    create_table :inventory_spreadsheets_server_databases do |t|
      t.integer :server_database_id
      t.integer :inventory_spreadsheet_id
    end
  end
end
