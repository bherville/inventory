class CreateServerRelationships < ActiveRecord::Migration
  def change
    create_table :server_relationships do |t|
      t.integer :server_primary_id
      t.integer :server_secondary_id
      t.integer :port

      t.timestamps
    end
  end
end
