class CreateServerDatabases < ActiveRecord::Migration
  def change
    create_table :server_databases do |t|
      t.integer :server_id
      t.integer :database_id

      t.timestamps
    end
  end
end
