class ChangeGenerationCompleteInventorySpreadsheets < ActiveRecord::Migration
  def up
    rename_column :inventory_spreadsheets, :generated_completed, :generation_completed_at
  end

  def down
    rename_column :inventory_spreadsheets, :generation_completed_at, :generated_completed
  end
end
