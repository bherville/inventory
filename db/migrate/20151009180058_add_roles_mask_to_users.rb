class AddRolesMaskToUsers < ActiveRecord::Migration
  def change
    add_column :users, :roles_mask, :integer, :default => User.mask_for(User::DEFAULT_ROLES)
  end
end
