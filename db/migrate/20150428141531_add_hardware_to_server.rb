class AddHardwareToServer < ActiveRecord::Migration
  def change
    add_column :servers, :data_center_id, :integer
    add_column :servers, :environment_id, :integer
    add_column :servers, :tenant_space_id, :integer
    add_column :servers, :zone_id, :integer
  end
end
