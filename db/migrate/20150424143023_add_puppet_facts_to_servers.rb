class AddPuppetFactsToServers < ActiveRecord::Migration
  def change
    add_column :servers, :puppet_facts, :text
  end
end
