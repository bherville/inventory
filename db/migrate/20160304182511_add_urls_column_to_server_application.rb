class AddUrlsColumnToServerApplication < ActiveRecord::Migration
  def change
    add_column :server_applications, :urls, :text
  end
end
