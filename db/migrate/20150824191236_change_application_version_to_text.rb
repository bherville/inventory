class ChangeApplicationVersionToText < ActiveRecord::Migration
  def up
    change_column :application_versions, :application_version, :text
  end

  def down
    change_column :application_versions, :application_version, :string
  end
end
