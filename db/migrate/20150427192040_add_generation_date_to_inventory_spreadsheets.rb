class AddGenerationDateToInventorySpreadsheets < ActiveRecord::Migration
  def change
    add_column :inventory_spreadsheets, :generated_at, :timestamp
    add_column :inventory_spreadsheets, :generated_completed, :timestamp
  end
end
