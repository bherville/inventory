class AddDiagramKeyToInventoryDiagram < ActiveRecord::Migration
  def self.up
    add_attachment :inventory_diagrams, :diagram_key
  end

  def self.down
    remove_attachment :inventory_diagrams, :diagram_key
  end
end
