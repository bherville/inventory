class CreateServerApplications < ActiveRecord::Migration
  def change
    create_table :server_applications do |t|
      t.integer :server_id
      t.integer :application_id
      t.integer :user_id
      t.text :description

      t.timestamps
    end
  end
end
