class AddServerStatsToServers < ActiveRecord::Migration
  def change
    add_column :servers, :processor_count, :integer
    add_column :servers, :memory_size, :decimal
    add_column :servers, :total_disk_space, :integer, :limit => 5
  end
end
