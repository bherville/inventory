class CreateInventorySpreadsheets < ActiveRecord::Migration
  def self.up
    create_table :inventory_spreadsheets do |t|
      t.integer :user_id

      t.timestamps
    end

    add_attachment :inventory_spreadsheets, :spreadsheet
  end

  def self.down
    drop_table :inventory_spreadsheets
  end
end
