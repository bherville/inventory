class CreateInventorySpreadsheetsServerRelationships < ActiveRecord::Migration
  def change
    create_table :inventory_spreadsheets_server_relationships do |t|
      t.integer :server_relationship_id
      t.integer :inventory_spreadsheet_id
    end
  end
end
