var process_inventory_jobs_interval_id;
var last_api_completed_jobs = -1;
var jobs;


$(document).ready(function() {
    var in_progress_jobs = get_inventory_jobs_working_or_queued_count();
    var complete_jobs = get_inventory_jobs_complete_count();



    if (in_progress_jobs > 0 || complete_jobs > 0) {
        console.log("Starting to check job status");
        jobs = get_inventory_jobs_by_session();


        process_inventory_jobs_interval_id = setInterval(function () {
            console.log("Checking job status");

            if (last_api_completed_jobs == -1) {
                console.log("This is the first check");
                do_process_inventory_jobs(jobs);
            }

            else {
                var current_api_completed_jobs = get_inventory_jobs_complete_count();
                console.log(current_api_completed_jobs + " jobs left to process");
                console.log(jobs.length + " local jobs left to be alerted");

                if (current_api_completed_jobs != jobs.count) {
                    console.log("New jobs are in process");

                    do_process_inventory_jobs();
                }
            }

            last_api_completed_jobs = get_inventory_jobs_complete_count();
        }, 4000);
    }

    if ($('#inventory_table').length) {
        inventory_table_page();
    }
});

function inventory_table_page()
{
    $('#inventory_table').dataTable(
        {
            "sScrollY": "400px",
            "bPaginate": true,
            "bDestroy": true,
            "iDisplayLength": 16,
            "aLengthMenu": [[15, 25, 50, 100, 200, -1], [15, 25, 50, 100, 200, "All"]]
        }).fnSort( [[0,'desc'] ] );
}


function do_process_inventory_jobs() {
    var to_delete = process_inventory_jobs(jobs);

    to_delete.forEach(function(entry) {
        jobs.splice(entry, 1);
    });

    if (jobs.length == 0) {
        console.log("No jobs left to process, killing check");
        clearInterval(process_inventory_jobs_interval_id)
    }
}


function get_inventory_jobs_working_or_queued_count() {
    var jobs = -1;

    $.ajax({
        url: "/api/jobs?filter_by=session:working_and_queued_count",
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            jobs = response.jobs.queued + response.jobs.working
        }
    });

    return jobs;
}


function get_inventory_jobs_complete_count() {
    var jobs = -1;

    $.ajax({
        url: "/api/jobs?filter_by=session:complete_count",
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            jobs = response.jobs.complete
        }
    });

    return jobs;
}

function get_inventory_jobs_by_session() {
    var THIRTY_MINUTES_MS = 1800000;

    var jobs = [];

    $.ajax({
        url: "/api/jobs?filter_by=session",
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            response.forEach(function(entry) {
                if (entry.status != "complete" || (((new Date) - Date.parse(entry.completed_at)) < THIRTY_MINUTES_MS)) {

                    var current_completed = get_completed_inventory_jobs();


                    if (already_completed_inventory_job(current_completed, entry) == false) {
                        jobs.push(entry);
                    }
                }
            });
        }
    });

    return jobs;
}

function process_inventory_jobs(jobs) {
    var index;
    var to_delete = [];
    var current_completed = get_completed_inventory_jobs();

    for	(index = 0; index < jobs.length; index++) {
        var job = jobs[index];

        if (already_completed_inventory_job(current_completed, job)) {
            to_delete.push(index)
        }

        else {
            var info = get_inventory_job_info(job);

            if (info.status == "complete") {
                var download_link_file_name = "<a href='" + info.path + "' >" + info.resource_type_human + " " + info.resource_id + "</a>";

                displayNotice('Inventory generation for ' + download_link_file_name + ' has been completed.');

                to_delete.push(index);
                add_completed_inventory_job(job);
            }
        }
    }

    return to_delete;
}

function get_inventory_job_info(job) {
    var inventory_info = null;

    $.ajax({
        url: "/api/jobs/" + job.id,
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            inventory_info = response;
        }
    });

    return inventory_info;
}

function get_completed_inventory_jobs() {
    var completed;
    var cookie_value = Cookies.get('inventories_completed');

    if (cookie_value === undefined) {
        completed = [];
    }
    else {
        completed = $.parseJSON(Cookies.get('inventories_completed'));
    }

    return completed;
}

function already_completed_inventory_job(current_completed, to_check) {
    var already = false;

    current_completed.forEach(function(entry) {
        var ent_array = entry.split("_");

        if (ent_array[0] == to_check.resource_type && ent_array[1] == to_check.id) {
            already = true
        }
    });

    return already;
}

function add_completed_inventory_job(job) {
    var current_completed = get_completed_inventory_jobs();

    current_completed.push(job.resource_type + "_" + job.id);
    Cookies.set('inventories_completed', JSON.stringify(current_completed));
}

function server_filter_show_hide() {
    if ($('#server_filter').is(":visible")) {
        //Hide
        $('#generate_link').show();
        $('#server_filter').hide();
    }

    else {
        //Show
        $('#generate_link').hide();
        $('#server_filter').show();

        $('#server_filter select').each(function( index, value ) {
            if (value.id != 'save_formats') {
                value.selectedIndex = -1;
            }
        });
    }
}
function start_processing_inventories(job_api_url, inventory_api_url, new_email_inventory_path) {
    var i = setInterval(function () {
        status_check(job_api_url, inventory_api_url, new_email_inventory_path);
    }, 4000);

    Cookies.set('status_check_interval', i);
}

function status_check(job_api_url, inventory_api_url, new_email_inventory_path) {
    $.ajax({
        url: job_api_url,
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            console.log(response);
            if (response.status == "complete") {
                job_complete(inventory_api_url, new_email_inventory_path);
            }
        }
    });
}

function get_inventory_info(inventory_api_url) {
    var inventory_info = null;

    $.ajax({
        url: inventory_api_url,
        cache: false,
        async: false,
        type: "get",
        success: function(response) {
            inventory_info = response;
        }
    });

    return inventory_info;
}

function job_complete(inventory_api_url, new_email_inventory_path) {
    var i = Cookies.get('status_check_interval');
    clearInterval(i);
    var inventory_info = get_inventory_info(inventory_api_url);

    console.log (inventory_info);

    window.open(inventory_info.inventory_file_url,'_blank');
    $(".new_inventory_msg").remove();

    var link = "<a href='" + inventory_info.inventory_path + "' >" + inventory_info.inventory_file_name + "</a>";
    var download_link = "<a href='" + inventory_info.inventory_file_url + "' >Download</a>";
    var send_email_link = "<a href='" + new_email_inventory_path + "' >Send via Email</a>";
    var download_link_file_name = "<a href='" + inventory_info.inventory_file_url + "' >" + inventory_info.inventory_file_name + "</a>";
    var generated_at = inventory_info.generated_at_formatted == null ? 'N/A' : inventory_info.generated_at_formatted;

    $("#inventory_table > tbody:last")
        .prepend('<tr id="' + inventory_info.inventory_id + '"><td>' + link + '</td><td>' + inventory_info.user + '</td><td>' + generated_at + '</td><td>' + download_link + " " + send_email_link + '</td></tr>');
    $("#"+ inventory_info.inventory_id).children("td:first").prepend('<span class="new_inventory_msg">NEW!</span>');

    inventory_table_page();

    displayNotice('Inventory generation for ' + download_link_file_name + ' has been completed.');
    add_completed_inventory_job(inventory_info.job);
}