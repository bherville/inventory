$(document).ready(function(){
    initializeAuditedTable();
});

function initializeAuditedTable(iDisplayLength, aLengthMenu)
{
    iDisplayLength = iDisplayLength || 10;
    aLengthMenu = aLengthMenu || [[5, 10, 15, 20, 50, -1], [5, 10, 15, 20, 50, "All"]];

    if ($('#audited_table').length) {
        $('#audited_table').dataTable(
            {
                "sScrollY": "200px",
                "bPaginate": true,
                "bDestroy": true,
                "iDisplayLength": iDisplayLength,
                "aLengthMenu": aLengthMenu
            }).fnSort([[0, 'desc']]);
    }
}