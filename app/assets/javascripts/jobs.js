$(document).ready(function(){
    initializeJobsTable();
});

function initializeJobsTable(iDisplayLength, aLengthMenu)
{
    iDisplayLength = iDisplayLength || 16;
    aLengthMenu = aLengthMenu || [[5, 10, 15, 20, 50, -1], [5, 10, 15, 20, 50, "All"]];

    if ($('#jobs_table').length) {
        $('#jobs_table').dataTable(
            {
                "sScrollY": "400px",
                "bPaginate": true,
                "bDestroy": true,
                "iDisplayLength": iDisplayLength,
                "aLengthMenu": aLengthMenu
            }).fnSort([[3, 'desc']]);
    }
}