class InventorySpreadsheet < ActiveRecord::Base
  attr_accessible :user, :spreadsheet, :servers, :application_versions, :server_databases, :server_relationships, :filter_params

  has_attached_file :spreadsheet, :default_url => '/images/:style/missing.png'
  validates_attachment_content_type :spreadsheet, :content_type => /application\/\w*.\w*.\w*\Z/

  has_and_belongs_to_many :servers
  has_and_belongs_to_many :application_versions
  has_and_belongs_to_many :server_databases
  has_and_belongs_to_many :server_relationships

  belongs_to :user
  has_one :job, as: :statusable

  serialize :puppet_facts
  serialize :filter_params

  before_create :add_user
  before_create :clean_filter_params

  SAVE_FORMATS = {
      :xls  => {
          :file_extension => 'xls'
      },
      :default  => :xls
  }

  def generate(save_format = SAVE_FORMATS[:default], force = false)
    raise 'Cannot regenerate this inventory: You attempted to generate an already generated InventorySpreadsheet!' unless self.generated_at.nil? || force

    self.generated_at = Time.now

    # Generate the path and create the working directory
    temp_dir = File.join(Rails.root, 'tmp', "#{self.class.to_s}_#{self.id}_#{Time.now.strftime('%12N')}")

    raise InventoryExceptions::DuplicateWorkingDirectory, "The working directory #{temp_dir} already exists!" if File.exist?(temp_dir)

    FileUtils.mkdir_p temp_dir
    logger.debug("Created working directory: #{temp_dir}")

    file_name = "#{self.generated_at.to_i.to_s}-inventory.#{SAVE_FORMATS[save_format.to_sym][:file_extension]}"
    file_path = File.join(temp_dir, file_name)

    application_family_applications = ((ApplicationFamily.select { |af| self.filter_params[:application_family][:name].include?(af.name) }).map { |af| af.application_family_applications }).flatten if self.filter_params && self.filter_params[:application_family] && self.filter_params[:application_family][:name]
    application_family_applications ||= (self.servers.map { |s| s.applications }).flatten.map { |a| a.application_family_applications}.flatten
    application_family_applications = ((application_family_applications.sort_by { |afa| afa.application_family.name }).sort_by { |afa| afa.application.name }).uniq
    book = InventoryCreator.generate(self.servers, APP_CONFIG, self.server_databases, self.server_relationships.select{ |sr| sr.server_primary && sr.server_secondary}, application_family_applications)

    if book
      book.write file_path

      File.open(file_path) do |f|
        self.spreadsheet = f
      end
    end

    self.generation_completed_at = Time.now

    self.save

    #FileUtils.rm_rf if temp_dir

    self.spreadsheet
  end

  def send_email(email_addresses)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.send_inventory(email_address, self.id, InventorySpreadsheet).deliver!
    end
  end

  def send_email_delayed(email_addresses)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.delay.send_inventory(email_address, self.id, InventorySpreadsheet)
    end
  end

  def send_email_delayed_filtered(email_addresses, params)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.delay.send_inventory_filtered(email_address, self.id, InventorySpreadsheet, params)
    end
  end

  def self.create_and_generate(attributes)
    inventory_spreadsheet = InventorySpreadsheet.create(attributes)

    inventory_spreadsheet.generate
    inventory_spreadsheet.save

    inventory_spreadsheet
  end

  def self.create_and_generate_async(attributes, save_format, options = {})
    inventory_spreadsheet = InventorySpreadsheet.create(attributes)

    job_id = InventorySpreadsheetWorker.perform_async(inventory_spreadsheet.id, save_format, options)

    Job.create(
        :job_id     => job_id,
        :statusable => inventory_spreadsheet,
        :session_id => options[:session_id],
        :user_id    => options[:user_id]
    )

    inventory_spreadsheet
  end

  def to_s
    "Inventory_#{self.created_at.strftime('%d%b%Y')}.xls"
  end

  def inventory
    self.spreadsheet
  end

  private
  def parse_email(email_addresses)
    if email_addresses.is_a? String
      to_addresses = email_addresses.split(',')
    elsif email_addresses.is_a? Array
      to_addresses = email_addresses
    else
      to_addresses = Array.new
    end

    to_addresses
  end

  def add_user
    self.user = User.find(ANONYMOUS_USER_ID) unless self.user

    self.user
  end

  def clean_filter_params
    if self.filter_params
      filter_params.delete('utf8')
      filter_params.delete('action')
      filter_params.delete('controller')
      filter_params.delete('commit')
    end
  end
end
