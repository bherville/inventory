class ServerRelationship < ActiveRecord::Base
  attr_accessible :port, :server_primary_id, :server_secondary_id, :server_primary, :server_secondary, :description

  APP_INFO_END_POINT = 'server_relationships'

  belongs_to :server_primary, :foreign_key => :server_primary_id, :class_name => 'Server'
  belongs_to :server_secondary, :foreign_key => :server_secondary_id, :class_name => 'Server'
  has_and_belongs_to_many :inventory_spreadsheets

  validates :server_primary_id, :presence => true
  validates :server_secondary_id, :presence => true

  validates_uniqueness_of :server_primary_id, :scope => :server_secondary_id

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        current_resource = nil

        current_resource = ServerRelationship.find_by_server_primary_id_and_server_secondary_id_and_port(resource.server_primary.id, resource.server_secondary.id, resource.port) if resource.server_primary && resource.server_secondary && resource.port

        if current_resource
          # Exists
          unless (current_resource.server_primary.name == resource.server_primary.name) &&
              (current_resource.server_secondary.name == resource.server_secondary.name) &&
              (current_resource.port == resource.port) &&
              (current_resource.description == resource.description)
            # Needs updates
            current_resource.server_primary   = resource.server_primary unless current_resource.server_primary.name == resource.server_primary.name
            current_resource.server_secondary = resource.server_secondary unless current_resource.server_secondary.name == resource.server_secondary.name
            current_resource.port             = resource.port unless current_resource.port == resource.port
            current_resource.description      = resource.description unless current_resource.description == resource.description

            current_resource.save
            created_updated << current_resource
          end
        else
          # Doesn't exist
          resource.save
          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      next unless resource.server_primary && resource.server_secondary

      to_delete << resource unless api_resources.any? {|r| (r.server_primary && r.server_secondary && ((r.server_primary.name.downcase == resource.server_primary.name.downcase) && (r.server_secondary.name.downcase == resource.server_secondary.name.downcase)) && r.port == resource.port)}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  def diagram_edge_label
    label = "Port #{self.port.to_s}"

    "#{label} - #{self.description}" unless self.description.nil? || self.description.empty?
  end

  private
  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource)

      server_primary    = Server.find_by_name(resource['server_primary']['name'])
      server_secondary  = Server.find_by_name(resource['server_secondary']['name'])

      next unless server_primary && server_secondary

      new_resources << new(
          :server_primary   => server_primary,
          :server_secondary => server_secondary,
          :port             => resource['port'],
          :description      => resource['description']
      )
    end

    new_resources
  end
end