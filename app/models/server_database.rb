class ServerDatabase < ActiveRecord::Base
  attr_accessible :database, :server

  APP_INFO_END_POINT = 'server_databases'

  validates_uniqueness_of :database_id, :scope => :server_id

  belongs_to :database
  belongs_to :server
  has_and_belongs_to_many :inventory_spreadsheets

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        next unless resource.database and resource.server

        current_resource = ServerDatabase.find_by_database_id_and_server_id(resource.database.id, resource.server.id)

        unless current_resource
          resource.save

          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      next unless resource.database && resource.server

      to_delete << resource unless api_resources.any? {|r| (r.database && r.server && (r.database.name.downcase == resource.database.name.downcase && r.database.dbms.downcase == resource.database.dbms.downcase) && r.server.name.downcase == resource.server.name.downcase)}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  private
  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource)

      next unless resource['database'] && resource['server'] && resource['database']['name'] && resource['server']['name']
      database  = Database.find_by_name_and_dbms(resource['database']['name'], resource['database']['dbms'])
      server    = Server.find_by_name(resource['server']['name'])

      new_resources << new(
          :database => database,
          :server   => server
      )
    end

    new_resources
  end
end
