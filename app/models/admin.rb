class Admin
  ADMIN_PAGES = {
      :users => {
          :title        => I18n.t('user.users'),
          :path         => Rails.application.routes.url_helpers.admin_users_path,
          :description  => I18n.t('admin.manage_users')
      },
      :jobs => {
          :title        => I18n.t('jobs.jobs'),
          :path         => Rails.application.routes.url_helpers.admin_jobs_path,
          :description  => I18n.t('admin.manage_jobs')
      }
  }
end