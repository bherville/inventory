class Server < ActiveRecord::Base
  audited except: :puppet_facts

  attr_accessible :name, :user_id, :puppet_facts, :zone

  validates :name,  :presence => true,
            :uniqueness => true

  APP_INFO_END_POINT = 'servers'

  belongs_to :user
  belongs_to :data_center
  belongs_to :environment
  belongs_to :tenant_space
  belongs_to :zone

  has_and_belongs_to_many :inventory_spreadsheets
  has_and_belongs_to_many :inventory_diagrams

  has_many :application_versions, :dependent => :destroy
  has_many :server_applications, :dependent => :destroy

  has_many :applications, :through => :server_applications

  has_many :server_relationships, :foreign_key => 'server_primary_id', :class_name => 'ServerRelationship', :dependent => :destroy

  has_many :server_databases, :dependent => :destroy
  has_many :databases, :through => :server_databases

  serialize :puppet_facts

  def self.create_and_update
    app_info_resources = get_app_info_resources
    puppetdb_resources = Puppetdb.retrieve_resources APP_CONFIG, Server
    created_updated = Array.new


    if puppetdb_resources
      puppetdb_resources.each do |resource_title, resource|
        next unless Server.active?(resource, app_info_resources)
        current_resource = find_by_name(resource_title)

        if current_resource
          # Exists
          unless (current_resource.puppet_facts == resource.puppet_facts) &&
              ((current_resource.data_center) && current_resource.data_center.name == resource.puppet_facts['in_datacenter']) &&
              ((current_resource.environment) && current_resource.environment.name == resource.puppet_facts['in_environment']) &&
              ((current_resource.tenant_space) && current_resource.tenant_space.name == resource.puppet_facts['in_tenant_space']) &&
              ((current_resource.zone) && current_resource.zone.name == resource.puppet_facts['in_zone']) &&
              (current_resource.processor_count == resource.puppet_facts['physicalprocessorcount']) &&
              (current_resource.memory_size == resource.puppet_facts['memorysize_mb']) &&
              (current_resource.total_disk_space == resource.puppet_facts['total_disk_space'])
            # Needs updates
            current_resource.puppet_facts     = resource.puppet_facts unless current_resource.puppet_facts == resource.puppet_facts
            current_resource.data_center      = DataCenter.find_or_create_by_name (resource.puppet_facts['in_datacenter'])      unless (current_resource.data_center && current_resource.data_center.name == resource.puppet_facts['in_datacenter'])
            current_resource.environment      = Environment.find_or_create_by_name (resource.puppet_facts['in_environment'])    unless (current_resource.environment && current_resource.environment.name == resource.puppet_facts['in_environment'])
            current_resource.tenant_space     = TenantSpace.find_or_create_by_name (resource.puppet_facts['in_tenant_space'])   unless (current_resource.tenant_space && current_resource.tenant_space.name == resource.puppet_facts['in_tenant_space'])
            current_resource.zone             = Zone.find_or_create_by_name (resource.puppet_facts['in_zone'])                  unless (current_resource.zone && current_resource.zone.name == resource.puppet_facts['in_zone'])
            current_resource.processor_count  = resource.puppet_facts['physicalprocessorcount'].to_i if resource.puppet_facts['physicalprocessorcount']
            current_resource.memory_size      = resource.puppet_facts['memorysize_mb'].to_f if resource.puppet_facts['memorysize_mb']
            current_resource.total_disk_space = resource.puppet_facts['total_disk_space'].to_f if resource.puppet_facts['total_disk_space']


            current_resource.save
            created_updated << current_resource
          end
        else
          # Doesn't exist
          resource.data_center      = DataCenter.find_or_create_by_name (resource.puppet_facts['in_datacenter'])
          resource.environment      = Environment.find_or_create_by_name (resource.puppet_facts['in_environment'])
          resource.tenant_space     = TenantSpace.find_or_create_by_name (resource.puppet_facts['in_tenant_space'])
          resource.zone             = Zone.find_or_create_by_name (resource.puppet_facts['in_zone'])
          resource.processor_count  = resource.puppet_facts['physicalprocessorcount'].to_i if resource.puppet_facts['physicalprocessorcount']
          resource.memory_size      = resource.puppet_facts['memorysize_mb'].to_f if resource.puppet_facts['memorysize_mb']
          resource.total_disk_space = resource.puppet_facts['total_disk_space'].to_i if resource.puppet_facts['total_disk_space']

          resource.save!
          created_updated << resource
        end
      end

      created_updated
    end
  end

  def self.remove_stale
    puppetdb_resources = (Puppetdb.retrieve_resources APP_CONFIG, Server).map { |n,r| r.name }
    app_info_resources = get_app_info_resources.keys

    resources = all.map { |r| r.name }


    to_delete = resources - puppetdb_resources
    to_delete = to_delete + (resources - app_info_resources)

    to_delete.each do |r|
      find_by_name(r).delete if r
    end

    to_delete
  end

  def self.generate_csv(records, options = {})
    CSV.generate(options) do |csv|
      csv << APP_CONFIG['puppetdb']['facts'].values

      records.each do |record|
        fields = Array.new

        APP_CONFIG['puppetdb']['facts'].each do |f,str|
          if record.puppet_facts
            fact = record.puppet_facts[f]

            if fact.class == String
              fields << fact.to_s.upcase
            end

            fields << record.puppet_facts[f].to_s.upcase if record.puppet_facts
          end
        end

        csv << fields if fields.count > 0
      end
    end
  end

  def diagram_title
    case self.zone.name
      when 'dmz'
        network_image = 'osa_server_dmz.png'
      when 'app'
        network_image = 'osa_server_app.png'
      when 'data'
        network_image = 'osa_server_data.png'
      else
        network_image = 'osa_server_unknown.png'
    end

    network_image_path = File.join(Rails.root, 'app', 'assets','images', 'nice_network', network_image)

    body = "#{body}<tr><td width='60' height='50' fixedsize='true'><IMG SRC='#{network_image_path}' scale='true'/></td></tr>"
    body = "#{body}<tr><td>#{self.puppet_facts['hostname'].upcase}</td></tr>"
    body = "#{body}<tr><td>#{self.puppet_facts['ipaddress_main']}</td></tr>"
    "#{body}<tr><td>--------------------------------------</td></tr>"
  end

  def diagram_body
    body = ''
    # Only use ApplicationVersions that have a non-nil application_version
    valid_application_versions = (self.application_versions.select { |av| av.application_version }).sort_by { |av| av.application.name}

    valid_application_versions.each do |av|
      version = av.application_version ? av.application_version.strip : nil
      app_type = av.application && av.application.app_type ? av.application.app_type.upcase.strip : nil
      name = av.application && av.application.name ? av.application.name.strip : nil

      body = "#{body}<tr><td>#{name} (#{app_type}"
      body = version ? "#{body} #{version})</td></tr>" : "#{body})</td></tr>"
    end
    body = "#{body}<tr><td>--------------------------------------</td></tr>" if valid_application_versions.count > 0

    # Map out the Applications from valid_application_versions
    valid_application_version_applications = valid_application_versions.map { |av| av.application}
    # Only use ServerApplications if self.application_versions did not have a valid application_version.
    valid_server_applications = (self.server_applications.reject{ |sa| valid_application_version_applications.include? sa.application }).sort_by { |sa| sa.application.name}

    valid_server_applications.each do |sa|
      app_type = sa.application && sa.application.app_type ? sa.application.app_type.upcase.strip : nil
      name = sa.application && sa.application.name ? sa.application.name.strip : nil

      if sa.application_version && !(sa.application_version.application_version.nil? || sa.application_version.application_version.empty?)
        version_string = sa.application_version.application_version.length > 17 ? " - #{sa.application_version.application_version[0..17]}..."  : " - #{sa.application_version.application_version}"
      else
        version_string = nil
      end

      body = "#{body}<tr><td>" if app_type || name
      body = "#{body}#{name}" if name
      body = "#{body} (#{app_type}#{version_string if version_string})" if app_type
      body = "#{body}</td></tr>" if app_type || name
    end

    body = "#{body}<tr><td>--------------------------------------</td></tr>" if valid_server_applications.count > 0

    self.databases.sort_by { |d| d.application.name }.each do |d|
      body = "#{body}<tr><td>#{d.application.name} - #{d.name}</td></tr>" if d.application
    end
    body = "#{body}<tr><td>--------------------------------------</td></tr>" if self.server_databases.count > 0

    body = "#{body}#{environment_string}"
    body = "#{body}<tr><td>--------------------------------------</td></tr>"
    "#{body}#{diagram_hardware_string}"
  end

  def diagram_options
    if self.zone
      Server.diagram_options(self.zone)
    else
      {}
    end
  end

  def self.diagram_options(zone)
    case zone.name
      when 'dmz'
        color = 'cornflowerblue'
      when 'app'
        color = 'mediumseagreen'
      when 'data'
        color = 'moccasin'
      else
        color = 'white'
    end

    {
        :shape      => :box,
        :color      => 'block',
        :fillcolor  => color,
        :style      => 'filled'
    }
  end

  def self.find_by_environment_attributes(params)
    servers = Server.all

    models = [
        TenantSpace,
        DataCenter,
        Environment,
        Zone
    ]

    valid_servers = servers

    models.each do |m|
      model_sym = m.to_s.underscore.to_sym
      params[model_sym][:name].delete('')

      next unless (params[model_sym] && params[model_sym][:name]) && (params[model_sym][:name].count > 0)

      environment_resources = m.find_all_by_name(params[model_sym][:name])

      temp_valid_servers = Array.new

      valid_servers.each do |s|
        next unless s.send(model_sym)

        server_attribute_name = s.send(model_sym)


        temp_valid_servers << s if environment_resources.include?(server_attribute_name)
      end

      valid_servers = temp_valid_servers
    end


    valid_applications = nil

    if params[:application_family] && params[:application_family][:name]
      temp_valid_servers = Array.new


      valid_applications = ApplicationFamily.find_by_name(params[:application_family][:name]).applications

      valid_servers.each do |s|
        next unless s.send(:applications)

        s.applications.each do |sa|
          temp_valid_servers << s if valid_applications.include?(sa)
        end
      end

      valid_servers = temp_valid_servers
    end


    if params[:applications]
      params[:applications][:name].delete('')
      if params[:applications][:name].count > 0
        temp_valid_servers = Array.new

        if valid_applications
          environment_resources = valid_applications.select { |a| params[:applications][:name].include?(a.name) }
        else
          environment_resources = Application.find_all_by_name(params[:applications][:name])
        end

        valid_servers.each do |s|
          next unless s.send(:applications)

          s.applications.each do |sa|
            temp_valid_servers << s if environment_resources.include?(sa)
          end
        end

        valid_servers = temp_valid_servers
      end
    end


    valid_servers
  end

  def self.find_by_environment_attributes_to_params(tenant_space, data_center, environment, zone, application, application_family)
    {
        :tenant_space           => { :name => tenant_space ? tenant_space.split(',') : Array.new },
        :data_center            => { :name => data_center ? data_center.split(',') : Array.new },
        :environment            => { :name => environment ? environment.split(',') : Array.new },
        :zone                   => { :name => zone ? zone.split(',') : Array.new },
        :application            => { :name => application ? application.split(',') : Array.new },
        :application_families   => { :name => application_family ? application_family.split(',') : Array.new }
    }
  end

  def to_s
    self.name
  end

  private
  def diagram_hardware_string
    hardware_array = Array.new

    if self.puppet_facts['memorysize']
      memory = /^[0-9]+(.[0-9]+)?/.match(self.puppet_facts['memorysize'])[0].to_f

      memory = memory.ceil
    end


    hardware_array << "#{self.puppet_facts['processorcount']}CPU" if self.puppet_facts['processorcount']
    hardware_array << "#{memory}GB" if memory
    hardware_array << "#{self.puppet_facts['total_disk_space_gb'].gsub(' ','')}" if self.puppet_facts['total_disk_space_gb']


    hardware_str = '<tr><td>'
    hardware_count = hardware_array.count
    hardware_array.each_with_index do |hardware, index|
      hardware_str = "#{hardware_str}#{hardware}"

      hardware_str = "#{hardware_str}/" if (index < (hardware_count - 1))
    end

    "#{hardware_str}</td></tr>"
  end

  def environment_string
    environment_array = Array.new

    environment_array << "#{self.tenant_space.name}" if self.tenant_space
    environment_array << "#{self.data_center.name.capitalize}" if self.data_center
    environment_array << "#{self.environment.name.capitalize}" if self.environment
    environment_array << "#{self.zone.name.upcase} Zone" if self.zone


    environment_str = ''
    environment_count = environment_array.count
    environment_array.each_with_index do |environment, index|
      environment_str = "#{environment_str}<tr><td>#{environment}</td></tr>"

      environment_str = "#{environment_str}" if (index < (environment_count - 1))
    end

    environment_str
  end

  def self.get_app_info_resources
    attr = {
        :name              => 'name',
    }

    AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, Server, attr, 'name'
  end

  def self.active?(resource, app_info_resources)
    app_info_resources.keys.include? resource.name
  end
end
