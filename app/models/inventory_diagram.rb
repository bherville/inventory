class InventoryDiagram < ActiveRecord::Base
  attr_accessible :user_id, :user, :servers, :name, :filter_params

  belongs_to :user
  has_and_belongs_to_many :servers
  has_many :server_relationships, :through => :servers
  has_one :job, as: :statusable

  validates :servers, :presence => true

  has_attached_file :diagram, :default_url => '/images/:style/missing.png'
  validates_attachment_content_type :diagram, :content_type => /(application\/pdf)|(image\/png)|(image\/jpg)|(image\/jpeg)|(image\/svg)/

  serialize :filter_params

  before_create :add_user
  before_create :clean_filter_params

  include Magick

  SAVE_FORMATS = {
      :png  => {
          :file_extension => 'png'
      },
      :jpg  => {
          :file_extension => 'jpg'
      },
      :default  => :png
  }

  def generate(save_format = SAVE_FORMATS[:default])
    raise 'Cannot regenerate this inventory: You attempted to generate an already generated InventoryDiagram!' unless self.generated_at.nil? || !self.diagram.exists?
    self.generated_at = Time.now

    # Instantiate the key graph
    key_graph = GraphViz::new(:G)
    key_graph['compound'] = 'true'
    key_graph.edge['lhead'] = ''
    key_graph.edge['ltail'] = ''
    key_graph[:rankdir] = 'LR'

    # Instantiate the server graph
    server_graph = GraphViz::new(:G, :use => 'neato', :overlap => 'scalexy')
    server_graph['compound'] = 'true'
    server_graph.edge['lhead'] = ''
    server_graph.edge['ltail'] = ''
    server_graph[:splines] = true
    server_graph[:rankdir] = 'LR'
    server_graph[:sep] = '+8;'

    # Add the key sub-graph
    add_key_to_graph(key_graph)
    # Add the server sub-graph
    add_servers_to_graph(server_graph)

    # Generate the path and create the working directory
    temp_dir = File.join(Rails.root, 'tmp', "#{self.class.to_s}_#{self.id}_#{Time.now.strftime('%12N')}")

    raise InventoryExceptions::DuplicateWorkingDirectory, "The working directory #{temp_dir} already exists!" if File.exist?(temp_dir)

    FileUtils.mkdir_p temp_dir
    logger.debug("Created working directory: #{temp_dir}")

    # Generate the path to the various temporary image files.
    temp_key_graph = File.join(temp_dir, "key_graph.#{SAVE_FORMATS[save_format.to_sym][:file_extension]}")
    temp_server_graph = File.join(temp_dir, "server_graph.#{SAVE_FORMATS[save_format.to_sym][:file_extension]}")
    temp_combined_graph = File.join(temp_dir ,"diagram_tmp.#{SAVE_FORMATS[save_format.to_sym][:file_extension]}")
    temp_combined_graph_with_header = File.join(temp_dir ,"#{(self.generated_at.to_f * 1000).to_i.to_s}-diagram.#{SAVE_FORMATS[save_format.to_sym][:file_extension]}")
    temp_header = File.join(temp_dir ,"header.png")
    logger.debug("Setting temp key graph path: #{temp_key_graph}")
    logger.debug("Setting temp server graph path: #{temp_server_graph}")
    logger.debug("Setting temp combined graph path: #{temp_combined_graph}")


    # Generate the graphs
    key_graph.output(save_format => temp_key_graph)
    server_graph.output(save_format => temp_server_graph)

    #Wait so the files are written
    sleep 1

    raise InventoryExceptions::NoGraph, "The graph #{temp_key_graph} does not exist!" unless File.exist?(temp_key_graph)
    raise InventoryExceptions::NoGraph, "The graph #{temp_server_graph} does not exist!" unless File.exist?(temp_server_graph)

    # Retrieve information about the server's graph
    #server_graph_image = Magick::Image::read(temp_server_graph).first

    # Resize the key graph to be 65% the size of the server graph
    resize_key_graph_image = ImageList.new(temp_key_graph)


    #width = server_graph_image.columns.to_i*0.65
    #height = server_graph_image.rows.to_i*0.65

    #logger.debug("Width: #{width}")
    #logger.debug("Height: #{height}")

    #width = 600.00 if width > 600.00
    #height = 300.00 if height > 300.00


    #resize_key_graph_image = resize_key_graph_image.resize_to_fit(width, height)
    resize_key_graph_image.write(temp_key_graph)


    #Wait so the files are written
    sleep 1

    # If the number of servers associated with this InventoryDiagram is not greater than 1 then make sure the graph is valid. If there are no servers than a graph less than 10KB is probably valid.
    if self.servers.count > 0
      raise InventoryExceptions::InvalidGraph, "The graph #{temp_key_graph} is not > 100KB so it is probably invalid!" unless (File.size(temp_key_graph) > 10000)
      raise InventoryExceptions::InvalidGraph, "The graph #{temp_server_graph} is not > 100KB so it is probably invalid!" unless (File.size(temp_key_graph) > 10000)
    end

    # Setup convert and montage
    montage_path = AppConfig.get(%w(inventory_diagram montage_path), APP_CONFIG)
    convert_path = AppConfig.get(%w(inventory_diagram convert_path), APP_CONFIG)

    montage_path ||= 'montage'
    convert_path ||= 'convert'




    # Combine the two graphs into one file/image
    montage_command = "#{montage_path} -alpha on -background white -mode concatenate -gravity SouthEast #{temp_server_graph} #{temp_key_graph} #{temp_combined_graph}"
    logger.debug("Montage command #{montage_command}")

    status = Open4::popen4(montage_command) do |pid, stdin, stdout, stderr|
      logger.info("Montage command output #{stdout.read.strip}")
      logger.info("Montage command error output #{stderr.read.strip}") if stderr
    end
    logger.debug("Montage command Open5 status output #{status.inspect}")

    #Wait so the files are written
    sleep 1



    # Create Header
    # Retrieve information about the server's graph
    temp_combined_graph_image = Magick::Image::read(temp_combined_graph).first
    width = temp_combined_graph_image.columns.to_i
    height = 200
    resize_key_graph_image.write(temp_key_graph)

    header_text = 'CGI (Virtualized Hosting Infrastructure)\n'
    header_filter_params_text = self.filter_params_title
    header_text = "#{header_text}#{header_filter_params_text}\n" if header_filter_params_text
    header_text = "#{header_text}#{Time.now.strftime('%-d %B %Y')}"

    convert_command = "#{convert_path} -size #{width}x#{height} gradient:goldenrod4-SteelBlue4 -gravity center -font Candice -pointsize 50 -fill black -annotate 0 '#{header_text}' #{temp_header}"
    logger.debug("Convert command #{convert_command}")

    status = Open4::popen4(convert_command) do |pid, stdin, stdout, stderr|
      logger.info("Convert command output #{stdout.read.strip}")
      logger.info("Convert command error output #{stderr.read.strip}") if stderr
    end
    logger.debug("Convert command Open5 status output #{status.inspect}")

    #Wait so the files are written
    sleep 1

    # Add header
    montage_command = "#{montage_path} #{temp_header} #{temp_combined_graph} -mode Concatenate -tile 1x2 #{temp_combined_graph_with_header}"

    logger.debug("Montage command #{montage_command}")

    status = Open4::popen4(montage_command) do |pid, stdin, stdout, stderr|
      logger.info("Montage command output #{stdout.read.strip}")
      logger.info("Montage command error output #{stderr.read.strip}") if stderr
    end
    logger.debug("Montage command Open5 status output #{status.inspect}")

    #Wait so the files are written
    sleep 1

    # Add first border
    convert_command = "#{convert_path} -border 10x10 -bordercolor black #{temp_combined_graph_with_header} #{temp_combined_graph}"

    logger.debug("Convert command #{convert_command}")

    status = Open4::popen4(convert_command) do |pid, stdin, stdout, stderr|
      logger.info("Convert command output #{stdout.read.strip}")
      logger.info("Convert command error output #{stderr.read.strip}") if stderr
    end
    logger.debug("Convert command Open5 status output #{status.inspect}")

    #Wait so the files are written
    sleep 1

    # Add second border
    convert_command = "#{convert_path} -border 75x75 -bordercolor white  #{temp_combined_graph} #{temp_combined_graph_with_header}"

    logger.debug("Convert command #{convert_command}")

    status = Open4::popen4(convert_command) do |pid, stdin, stdout, stderr|
      logger.info("Convert command output #{stdout.read.strip}")
      logger.info("Convert command error output #{stderr.read.strip}") if stderr
    end
    logger.debug("Convert command Open5 status output #{status.inspect}")

    #Wait so the files are written
    sleep 1





    # If the number of servers associated with this InventoryDiagram is not greater than 1 then make sure the graph is valid. If there are no servers than a graph less than 10KB is probably valid.
    if self.servers.count > 0
      raise InventoryExceptions::InvalidGraph, "The graph #{temp_combined_graph_with_header} is not > 100KB so it is probably invalid!" unless (File.size(temp_combined_graph_with_header) > 10000)
    end

    # Add the final image to this InventoryDiagram
    File.open(temp_combined_graph_with_header) do |f|
      self.diagram = f
    end

    self.generation_completed_at = Time.now
    # Save this InventoryDiagram
    self.save

    FileUtils.rm_rf temp_dir if(AppConfig.get(%w(inventory_diagram debug), APP_CONFIG) || AppConfig.get(%w(inventory_diagram debug), APP_CONFIG) == 'true')

    self.diagram
  end

  def filter_params_title
    header_filter_params_text = nil

    self.filter_params.each do |param, values|
      next unless values.is_a?(Hash)

      attributes = values['name']
      attributes ||= values[:name]

      next unless attributes

      attributes.each do |v|
        resource = param.to_s.humanize.split.map(&:capitalize).join('').singularize.constantize.find_by_name(v)

        header_filter_params_text = "#{header_filter_params_text} #{resource.display_format}"
      end
    end

    header_filter_params_text
  end

  def send_email(email_addresses)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.send_inventory(email_address, self.id, InventoryDiagram).deliver!
    end
  end

  def send_email_delayed(email_addresses)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.delay.send_inventory(email_address, self.id, InventoryDiagram)
    end
  end

  def send_email_delayed_filtered(email_addresses, params)
    to_addresses = parse_email email_addresses

    to_addresses.each do |email_address|
      InventoryMailer.delay.send_inventory_filtered(email_address, self.id, InventoryDiagram, params)
    end
  end

  def self.create_and_generate(attributes)
    inventory_diagram = InventoryDiagram.create(attributes)

    inventory_diagram.generate
    inventory_diagram.save

    inventory_diagram
  end

  def self.create_and_generate_async(attributes, save_format, options = {})
    inventory_diagram = InventoryDiagram.create(attributes)

    job_id = InventoryDiagramWorker.perform_async(inventory_diagram.id, save_format, options)

    Job.create(
        :job_id     => job_id,
        :statusable => inventory_diagram,
        :session_id => options[:session_id],
        :user_id => options[:user_id]
    )

    inventory_diagram
  end

  def to_s
    self.diagram_file_name
  end

  def inventory
    self.diagram
  end

  def diagram_file_name_without_extension
    File.basename(self.diagram_file_name, File.extname(self.diagram_file_name))
  end

  def add_key_to_graph(graph_g)
    zones = (((servers.select { |s| s.zone }).map { |s| s.zone }).sort_by { |z| z.name}).uniq

    zone_nodes = Hash.new
    key_graph = graph_g.add_graph( "cluster_key" )
    key_graph[:fontname] = 'Helvetica'
    key_graph[:label] = 'Key'
    key_graph[:labelloc] = 't'
    key_graph[:rankdir] = 'LR'

    graph_cluster_key = key_graph.add_graph('graph_cluster_key')
    graph_cluster_key[:fontname] = 'Helvetica'
    graph_cluster_key[:label] = 'Key'
    graph_cluster_key[:labelloc] = 't'
    graph_cluster_key[:rankdir] = 'LR'

    graph__11 = graph_cluster_key.add_graph( "_11" )
    graph__11[:fontname] = 'Helvetica'
    graph__11[:label] = 'Key'
    graph__11[:labelloc] = 't'
    graph__11[:rank] = 'source'
    graph__11[:rankdir] = 'LR'

    graph__12 = graph_cluster_key.add_graph( "_12" )
    graph__12[:fontname] = 'Helvetica'
    graph__12[:label] = 'Key'
    graph__12[:labelloc] = 't'
    graph__12[:rank] = 'source'
    graph__12[:rankdir] = 'LR'


    count = 1
    zones.each do |z|
      server = Server.new(
          :name          => "OCS1TESTSERVERLPR0#{count}",
          :puppet_facts  => {
              'hostname'             => "OCS1TESTSERVERLPR0#{count}",
              'ipaddress_main'       => '172.22.72.36',
              'processorcount'       => '4',
              'memorysize'           => '8',
              'total_disk_space_gb'  => '512GB'
          },
          :zone          => z
      )


      server_hash = server.diagram_options
      server_hash[:label] = "<<TABLE border='0' cellborder='0'>#{server.diagram_title}#{server.diagram_body}</TABLE>>"


      node = graph_cluster_key.add_nodes("#{server.name}", server_hash)
      key = graph__11.add_nodes( "#{z.name}", :label => "#{z.name.upcase} Zone", :peripheries => '', :shape => 'plaintext', :style => 'solid', :height => '')
      graph_cluster_key.add_edges(key, node, :style => 'invis')

      zone_nodes[z.name.to_sym] = {
          :node => node,
          :key  => key
      }

      count = count + 1
    end

    if server_relationships?
      server1 = Server.new(
          :name          => "OCS1TESTSERVERLPR01",
          :puppet_facts  => {
              'hostname'             => 'OCS1TESTSERVERLPR01',
              'ipaddress_main'       => '172.22.72.36',
              'processorcount'       => '4',
              'memorysize'           => '8',
              'total_disk_space_gb'  => '512GB'
          },
          :zone          => Zone.find_by_name('dmz')
      )

      server2 = Server.new(
          :name          => "OCS1TESTSERVERLPR02",
          :puppet_facts  => {
              'hostname'             => 'OCS1TESTSERVERLPR02',
              'ipaddress_main'       => '172.22.74.36',
              'processorcount'       => '4',
              'memorysize'           => '8',
              'total_disk_space_gb'  => '512GB'
          },
          :zone          => Zone.find_by_name('data')
      )

      server1_hash = server1.diagram_options
      #server1_hash[:label] = "#{server1.diagram_title}\n#{server1.diagram_body}\n"
      server1_hash[:label] = "<<TABLE border='0' cellborder='0'>#{server1.diagram_title}#{server1.diagram_body}</TABLE>>"

      server2_hash = server2.diagram_options
      #server2_hash[:label] = "#{server2.diagram_title}\n#{server2.diagram_body}\n"
      server2_hash[:label] = "<<TABLE border='0' cellborder='0'>#{server2.diagram_title}#{server2.diagram_body}</TABLE>>"


      server_primary_node = graph_cluster_key.add_nodes('server_primary', server1_hash)
      graph_cluster_key.add_nodes('server_secondary', server2_hash)
      graph_cluster_key.add_edges('server_primary', 'server_secondary')
      key = graph__12.add_nodes( 'relationship', :label => "Server Relationship (Web server to Database server)\r", :peripheries => '', :shape => 'plaintext', :style => 'solid')
      graph_cluster_key.add_edges(key, server_primary_node, :style => 'invis')
    end
  end

  def add_servers_to_graph(graph_g)
    server_nodes = Hash.new
    server_relationship_edges = Hash.new

    # Process each server
    servers.each do |s|
      server_nodes[s.name] = {
          :node   => graph_g.add_nodes("<<TABLE border='0' cellborder='0'>#{s.diagram_title}#{s.diagram_body}</TABLE>>", s.diagram_options),
          :server => s
      }
    end


    # Process the ServerRelationships (i.e. the edges)
    server_nodes.each do |key,sn|
      sn[:server].server_relationships.each do |sr|
        next unless sr.server_secondary && server_nodes[sr.server_secondary.name]

        server_primary = sn
        server_secondary = server_nodes[sr.server_secondary.name]

        server_relationship_edges_key = "#{server_primary[:server].name}_#{server_secondary[:server].name}"

        server_relationship_edges[server_relationship_edges_key] = graph_g.add_edges(server_primary[:node], server_secondary[:node]) unless server_relationship_edges.keys.include?(server_relationship_edges_key)
      end
    end
  end

  private
  def file_name
    self.name.nil? ? "#{self.generated_at.strftime('%m%d%Y_%M%S%H')}" : "#{self.name}_#{self.generated_at.strftime('%m%d%Y_%M%S%H')}"
  end

  def parse_email(email_addresses)
    if email_addresses.is_a? String
      to_addresses = email_addresses.split(',')
    elsif email_addresses.is_a? Array
      to_addresses = email_addresses
    else
      to_addresses = Array.new
    end

    to_addresses
  end

  def server_relationships?
    relationships = false

    server_relationships.each do |sr|
      relationships = true if (servers.include?(sr.server_primary) && servers.include?(sr.server_secondary))
    end

    relationships
  end

  def add_user
    self.user = User.find(ANONYMOUS_USER_ID) unless self.user

    self.user
  end

  def clean_filter_params
    if self.filter_params
      filter_params.delete('utf8')
      filter_params.delete('action')
      filter_params.delete('controller')
      filter_params.delete('commit')
    end
  end
end