class ApplicationVersionsInventorySpreadsheets < ActiveRecord::Base
  attr_accessible :application_version_id, :inventory_spreadsheet_id
end
