class ApplicationVersion < ActiveRecord::Base
  audited

  attr_accessible :application, :application_version, :server, :server_application

  APP_INFO_END_POINT = 'application_versions'

  validates :server_application_id, :presence => true, :uniqueness => true

  belongs_to :server_application

  has_one :application, :through => :server_application
  has_one :server, :through => :server_application

  has_and_belongs_to_many :inventory_spreadsheets

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        application = Application.find_by_name(resource.application.name)
        server = Server.find_by_name(resource.server.name)
        server_application = ServerApplication.find_by_application_id_and_server_id(application.id, server.id)

        next unless server_application

        current_resource = ApplicationVersion.find_by_server_application_id(server_application.id)

        if current_resource
          unless current_resource.application_version == resource.application_version
            current_resource.application_version = resource.application_version

            current_resource.save
            created_updated << current_resource
          end
        else
          resource.save!
          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      to_delete << resource unless api_resources.any? {|r| ((r.application && resource.application && r.application.name.downcase == resource.application.name.downcase) && (r.server && resource.server && r.server.name.downcase == resource.server.name.downcase))}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  def self.generate_csv(records, options = {})
    CSV.generate(options) do |csv|
      csv << ['Name', 'Server', 'Type', 'Version', 'Sends Emails', 'Major Application']

      records.each do |record|
        csv << [record.application.name.humanize, record.server.name.capitalize, record.application.app_type.capitalize, record.application_version, (record.application.sends_emails? ? 'Yes' : 'No'), (record.application.major_application? ? 'Yes' : 'No')]
      end
    end
  end

  def to_s
    str = "#{self.application}"
    str += " - #{self.application_version}" if self.application_version

    str
  end

  def audit_association
    [self.server]
  end

  private
  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource) && resource['application'] && resource['server']

      application = Application.find_by_name(resource['application']['name'])
      server      = Server.find_by_name(resource['server']['name'])

      next unless application && server
      server_application = ServerApplication.find_by_application_id_and_server_id(application.id, server.id)

      if server_application
        application_version = resource['application_version'].strip if resource['application_version']

        new_resources << new(
            :application_version  => application_version,
            :server_application   => server_application,
        )
      end
    end

    new_resources
  end
end
