class Job < ActiveRecord::Base
  attr_accessible :job_id, :statusable, :session_id, :completed_at, :status, :user_id

  validates :job_id, :presence => true

  belongs_to :statusable, polymorphic: true
  belongs_to :user

  before_create :update_status

  STATUS_COMPLETE = 'complete'
  STATUS_QUEUED   = 'queued'

  def update_status
    self.status = Sidekiq::Status::status(self.job_id)
  end
end
