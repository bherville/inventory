class Database < ActiveRecord::Base
  attr_accessible :name, :dbms, :servers, :application

  APP_INFO_END_POINT = 'databases'

  validates :name, :presence => true

  validates_uniqueness_of :name, :scope => :dbms

  before_save :fix_case

  has_many :server_databases, :dependent => :destroy
  has_many :servers, through: :server_databases
  belongs_to :application

  def full_name
    "#{self.dbms} - #{self.name}"
  end

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        current_resource = Database.find_by_name_and_dbms(resource.name, resource.dbms)

        if current_resource
          # Check if Application is set
          if current_resource.application
            # Check if Application is set for the AppInfo resource
            if resource.application
              # Update current_resource if both Applications are not the same.
              unless (current_resource.application.name == resource.application.name) &&
                  (current_resource.application.app_type == resource.application.app_type) &&
                  (current_resource.application.sends_emails == resource.application.sends_emails) &&
                  (current_resource.application.path == resource.application.path) &&
                  (current_resource.application.major_application == resource.application.major_application)
                current_resource.application.name               = resource.application.name
                current_resource.application.app_type           = resource.application.app_type
                current_resource.application.sends_emails       = resource.application.sends_emails
                current_resource.application.path               = resource.application.path
                current_resource.application.major_application  = resource.application.major_application

                current_resource.application.save
                created_updated << current_resource
              end
            else
              # Remove the association from current_resource
              current_resource.application = nil

              current_resource.save
              created_updated << current_resource
            end
          elsif resource.application
            application = Application.find_or_create_by_name(resource.application.name)

            current_resource.application = application
            current_resource.save
            created_updated << current_resource
          end
        else
          next unless resource.application
          application = Application.find_or_create_by_name(resource.application.name)
          resource.application = application
          resource.save

          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      to_delete << resource unless api_resources.any? {|r| (r.name.downcase == resource.name.downcase && r.dbms.downcase == resource.dbms.downcase)}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  private
  def fix_case
    self.dbms = self.dbms.downcase
  end

  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource)
      application = nil

      if resource['application']
        if resource['application']['active'] || resource['application']['ignore_active']
          attr = {
              :name              => resource['application']['name'],
              :app_type          => resource['application']['app_type'],
              :major_application => resource['application']['major_application'],
              :path              => resource['application']['path'],
              :sends_emails      => resource['application']['sends_emails']
          }

          application = Application.new(attr)
        end
      end

      new_resources << new(
        :name         => resource['name'],
        :dbms         => resource['dbms'],
        :application  => application
      )
    end

    new_resources
  end
end
