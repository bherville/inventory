class TenantSpace < ActiveRecord::Base
  attr_accessible :name

  PUPPET_NAME = 'in_tenant_space'

  validates :name, :presence    => true,
                   :uniqueness  => true

  has_many :servers
  belongs_to :user

  has_many :data_centers, :through => :servers, :uniq => true
  has_many :environments, :through => :servers, :uniq => true
  has_many :zones, :through => :servers, :uniq => true

  def self.create_and_update
    puppetdb_resouces = Puppetdb.retrieve_hardware_resources APP_CONFIG, TenantSpace
    created_updated = Array.new


    if puppetdb_resouces
      puppetdb_resouces.each do |resource_title, resource|
        current_resource = find_by_name(resource_title)

        unless current_resource
          resource.save
          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = (Puppetdb.retrieve_hardware_resources APP_CONFIG, TenantSpace).map { |n,r| r.name }

    resources = all.map { |r| r.name }

    to_delete = (resources - api_resources)

    to_delete.each do |r|
      find_by_name(r).delete
    end

    to_delete
  end

  def to_s
    self.name
  end

  def display_format
    self.name.upcase
  end
end
