class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user

    if user.has_role? :normal_user
      can :read, :all
    end

    if user.has_role? :admin
      can :manage, User
      can :manage, Job
    end

    if user.has_role? :inventory_diagram_generator
      can :create, [InventoryDiagram, Job]
      can :generate, [InventoryDiagram]
      can :new_email, [InventoryDiagram]
      can :send_via_email, [InventoryDiagram]
    end

    if user.has_role? :inventory_spreadsheet_generator
      can :create, [InventorySpreadsheet, Job]
      can :generate, [InventorySpreadsheet]
      can :new_email, [InventorySpreadsheet]
      can :send_via_email, [InventorySpreadsheet]
    end
  end
end
