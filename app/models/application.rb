class Application < ActiveRecord::Base
  attr_accessible :app_type, :major_application, :name, :path, :sends_emails, :user_id, :application_families

  validates :app_type,  :presence => true
  validates :name,      :presence => true,
                        :uniqueness => true

  APP_INFO_END_POINT = 'applications'

  belongs_to :user

  has_many :application_versions, :dependent => :destroy
  has_many :application_family_applications, :dependent => :destroy
  has_many :server_applications, :dependent => :destroy

  has_many :servers, :through => :server_applications
  has_many :application_families, through: :application_family_applications

  has_one :database, :dependent => :destroy

  def self.create_and_update
    created_updated = Array.new

    attr = {
        :name              => 'name',
        :app_type          => 'app_type',
        :major_application => 'major_application',
        :path              => 'path',
        :sends_emails      => 'sends_emails'
    }

    resources = AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, Application, attr, 'name'

    if resources
      resources.each do |resource_title, resource|
        current_resource = find_by_name(resource_title)

        if current_resource
          unless (current_resource.app_type == resource.app_type) &&
              (current_resource.major_application == resource.major_application) &&
              (current_resource.path == resource.path) &&
              (current_resource.sends_emails == resource.sends_emails)
            # Update the existing resource
            current_resource.app_type           = resource.app_type           unless current_resource.app_type == resource.app_type
            current_resource.major_application  = resource.major_application  unless current_resource.major_application == resource.major_application
            current_resource.path               = resource.path               unless current_resource.path == resource.path
            current_resource.sends_emails       = resource.sends_emails       unless current_resource.sends_emails == resource.sends_emails

            created_updated << current_resource
            current_resource.save
          end
        else
          created_updated << resource
          resource.save
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = (AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, Application, {:name => 'name'}, 'name').map { |n,r| r.name }

    resources = all.map { |r| r.name }

    to_delete = (resources - api_resources)

    to_delete.each do |r|
      find_by_name(r).delete
    end

    to_delete
  end

  def self.generate_csv(records, options = {})
    CSV.generate(options) do |csv|
      csv << ['Name', 'Type', 'Path', 'Sends Emails', 'Major Application']

      records.each do |record|
        csv << [record.name.humanize, record.app_type.capitalize, record.path, (record.sends_emails? ? 'Yes' : 'No'), (record.major_application? ? 'Yes' : 'No')]
      end
    end
  end

  def to_s
    self.name
  end

  def display_format
    self.name
  end
end
