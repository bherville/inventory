class ApplicationFamilyApplication < ActiveRecord::Base
  attr_accessible :application_family_id, :application_id, :application_family, :application

  validates :application_family_id, :presence => true
  validates :application_id, :presence => true
  validates_uniqueness_of :application_family_id, :scope => :application_id

  belongs_to :application
  belongs_to :application_family
  has_many :servers, through: :application

  APP_INFO_END_POINT = 'application_family_applications'

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        current_resource = nil

        current_resource = ApplicationFamilyApplication.find_by_application_family_id_and_application_id(resource.application_family.id, resource.application.id) if resource.application_family && resource.application

        if current_resource
          # Exists
          unless current_resource.application_family.name == resource.application_family.name && current_resource.application.name == resource.application.name
            # Needs updates
            current_resource.application_family = resource.application_family unless current_resource.application_family.name == resource.application_family.name
            current_resource.application        = resource.application unless current_resource.application.name == resource.application.name

            current_resource.save
            created_updated << current_resource
          end
        else
          # Doesn't exist
          resource.save
          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      next unless resource.application_family && resource.application

      to_delete << resource unless api_resources.any? {|r| (r.application_family && r.application && ((r.application_family.name.downcase == resource.application_family.name.downcase) && (r.application.name.downcase == resource.application.name.downcase)))}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  private
  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource)

      application_family  = ApplicationFamily.find_by_name(resource['application_family']['name'])
      application         = Application.find_by_name(resource['application']['name'])

      new_resources << new(
          :application_family => application_family,
          :application        => application
      )
    end

    new_resources
  end
end
