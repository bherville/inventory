class AdminAbility
  include CanCan::Ability
  def initialize(user)
    return unless user && user.has_role?(:admin)

    can :manage, :all
  end
end