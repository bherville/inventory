class ServerApplication < ActiveRecord::Base
  attr_accessible :description, :application_id, :server_id, :application, :server, :user, :urls
  
  APP_INFO_END_POINT = 'server_applications'

  validates :application_id, :presence => true
  validates :server_id, :presence => true
  validates_uniqueness_of :server_id, :scope => :application_id

  belongs_to :application
  belongs_to :server
  belongs_to :user

  has_one :application_version, :dependent => :destroy

  serialize :urls, Array

  def self.create_and_update
    resources = appinfo_json_to_resources(AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT)
    created_updated = Array.new

    if resources
      resources.each do |resource|
        application = Application.find_by_name(resource.application.name)
        server = Server.find_by_name(resource.server.name)
        current_resource = ServerApplication.find_by_application_id_and_server_id(application.id, server.id)

        if current_resource
          unless current_resource.description == resource.description &&
                current_resource.urls == resource.urls
            current_resource.description = resource.description
            current_resource.urls = resource.urls

            current_resource.save
            created_updated << current_resource
          end
        else
          resource.save!
          created_updated << resource
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = appinfo_json_to_resources((AppInfo.retrieve_resources_json APP_CONFIG, APP_INFO_END_POINT))

    resources = all

    to_delete = Array.new

    resources.each do |resource|
      to_delete << resource unless api_resources.any? {|r| ((r.application && resource.application && r.application.name.downcase == resource.application.name.downcase) && (r.server && resource.server && r.server.name.downcase == resource.server.name.downcase))}
    end

    to_delete.each do |r|
      r.delete
    end

    to_delete
  end

  def self.generate_csv(records, options = {})
    CSV.generate(options) do |csv|
      csv << %w(Server Application)

      records.each do |record|
        csv << [record.server.name.capitalize, record.application.name]
      end
    end
  end

  private
  def self.appinfo_json_to_resources(json)
    new_resources = Array.new

    json.each do |resource|
      next unless AppInfo.include?(APP_CONFIG, resource)

      next unless resource['application'] && resource['server'] && resource['application']['name'] && resource['server']['name']
      application = Application.find_by_name(resource['application']['name'])
      server      = Server.find_by_name(resource['server']['name'])

      if application && server
        new_resources << new(
            :application  => application,
            :server       => server,
            :description  => resource['description'],
            :urls         => resource['urls'],
            :user         => User.find(ANONYMOUS_USER_ID),
        )
      end
    end

    new_resources
  end
end
