class ApplicationFamily < ActiveRecord::Base
  attr_accessible :last_checked_at, :name, :ignore_active, :active, :applications, :application_ids

  validates :name, :presence => true,
                   :uniqueness => true

  has_many :application_family_applications, :dependent => :destroy

  has_many :applications, through: :application_family_applications

  belongs_to :user

  APP_INFO_END_POINT = 'application_families'

  def self.create_and_update
    created_updated = Array.new

    attr = {
        :name              => 'name',
    }

    resources = AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, ApplicationFamily, attr, 'name'

    if resources
      resources.each do |resource_title, resource|
        current_resource = find_by_name(resource_title)

        if current_resource
          unless current_resource.name == resource.name
            current_resource.name = resource.name unless current_resource.name == resource.name

            created_updated << current_resource
            current_resource.save
          end
        else
          created_updated << resource
          resource.save
        end
      end
    end

    created_updated
  end

  def self.remove_stale
    api_resources = (AppInfo.retrieve_resources APP_CONFIG, APP_INFO_END_POINT, ApplicationFamily, {:name => 'name'}, 'name').map { |n,r| r.name }

    resources = all.map { |r| r.name }

    to_delete = (resources - api_resources)

    to_delete.each do |r|
      find_by_name(r).delete
    end

    to_delete
  end

  def to_s
    self.name
  end

  def display_format
    self.name
  end
end
