class User < ActiveRecord::Base
  include Gravtastic
  gravtastic :secure => (AppConfig.get(%w(user_profiles secure), APP_CONFIG) ? AppConfig.get(%w(user_profiles secure), APP_CONFIG) : false)

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable,
         :timeoutable

  devise :omniauthable, :omniauth_providers => [:auth_man]

  include RoleModel

  DEFAULT_ROLES = [
      :normal_user
  ]

  ROLE_DESCRIPTIONS = {
      :admin                            => 'Can administrate the system.',
      :normal_user                      => 'Default role.',
      :inventory_diagram_generator      => 'Can generate InventoryDiagrams.',
      :inventory_spreadsheet_generator  => 'Can generate InventorySpreadsheets.'
  }
  roles_attribute :roles_mask
  roles ROLE_DESCRIPTIONS.keys

  before_create :add_default_roles

  attr_accessible :email, :fname, :lname, :password, :time_zone, :roles

  has_many :applications
  has_many :application_versions
  has_many :servers
  has_many :data_centers
  has_many :environments
  has_many :tenant_spaces
  has_many :zones
  has_many :server_applications

  has_many :inventory_spreadsheets
  has_many :inventory_diagrams

  has_many :jobs

  def to_s
    self.name
  end

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def full_name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : nil
  end

  def full_name_comma
    self.fname && self.lname ? "#{self.lname}, #{self.fname}" : nil
  end

  def name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : self.email
  end

  private
  def valid_roles
    if self.roles_mask.nil? || self.roles_mask < 1
      errors.add(:base, 'At least one role must be selected')
    end
  end

  def add_default_roles
    DEFAULT_ROLES.each do |role|
      self.roles << role unless self.has_role?(role)
    end

    AppConfig.get(%w(user_profiles default_roles), APP_CONFIG).each do |role|
      self.roles << role.to_sym unless self.has_role?(role.to_sym) || !User.valid_roles.include?(role.to_sym)
    end
  end
end
