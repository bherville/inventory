class InventorySpreadsheetWorker
  include Sidekiq::Worker

  def perform(resource_id, save_format, options)
    resource = InventorySpreadsheet.find resource_id

    resource.generate save_format

    if options['email_addresses']
      resource.send_email_delayed options['email_addresses'].split(',')
    end

    resource.save

    job = resource.job
    job.completed_at = Time.now
    job.status = 'complete'
    job.save
  end
end