module ApplicationHelper
  def user_string(resource)
    resource.user ? ((resource.user.name.nil? || resource.user.name.empty?) ? resource.user.email : resource.user.name) : t('general.not_applicable')
  end

  def branding_title
    title = ''

    title = "#{APP_CONFIG['branding']['title']} - " if APP_CONFIG['branding'] && APP_CONFIG['branding']['title']

    "#{title}#{t('inventory.inventory')}"
  end

  def filter_param_link(key, value)
    resource_class = key.to_s.humanize.split.map(&:capitalize).join('').singularize.constantize
    resource = resource_class.find_by_name(value)

    begin
      url_for(:controller => key.pluralize, :action => :show, :id => resource.id, :only_path => true)
    rescue
      nil
    end
  end

  def avatar_url(user, size = 80)
      user.gravatar_url(:size => size)
  end

  def show_avatar
    APP_CONFIG["user_profiles"].present? && APP_CONFIG["user_profiles"]["show_avatars"]
  end
end
