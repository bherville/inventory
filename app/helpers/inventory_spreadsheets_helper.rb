module InventorySpreadsheetsHelper
  def format_generated_at(inventory_spreadsheet)
    inventory_spreadsheet.generated_at.nil? ? t('general.not_applicable') : inventory_spreadsheet.generated_at.to_formatted_s(:long_ordinal)
  end
end
