module RegistrationsHelper
  def role_formatted(role)
    {
        :symbol       => role.to_sym,
        :name         => role.to_s.titleize,
        :description  => User::ROLE_DESCRIPTIONS[role]
    }
  end
end
