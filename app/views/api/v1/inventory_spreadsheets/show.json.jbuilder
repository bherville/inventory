json.generated_at @inventory_spreadsheet.generated_at
json.generated_at_formatted @inventory_spreadsheet.generated_at.nil? ? nil : @inventory_spreadsheet.generated_at.to_formatted_s(:long_ordinal)
json.spreadsheet_file_name @inventory_spreadsheet.spreadsheet_file_name
json.spreadsheet_file_size @inventory_spreadsheet.spreadsheet_file_size
json.spreadsheet_url @inventory_spreadsheet.spreadsheet.url
json.inventory_file_url @inventory_spreadsheet.spreadsheet.url
json.inventory_api_path api_inventory_spreadsheet_path(@inventory_spreadsheet)
json.inventory_path inventory_spreadsheet_path(@inventory_spreadsheet)
json.inventory_id @inventory_spreadsheet.id
json.inventory_file_name @inventory_spreadsheet.spreadsheet_file_name
json.user user_string(@inventory_spreadsheet)
json.job do
  json.partial! 'api/v1/jobs/job', job: @inventory_spreadsheet.job
end
