json.generated_at @inventory_spreadsheet.generated_at
json.generated_at_formatted @inventory_spreadsheet.generated_at.nil? ? nil : @inventory_spreadsheet.generated_at.to_formatted_s(:long_ordinal)
json.inventory_api_path api_inventory_spreadsheet_path(@inventory_spreadsheet)
json.inventory_path inventory_spreadsheet_path(@inventory_spreadsheet)
json.inventory_id @inventory_spreadsheet.id
json.user user_string(@inventory_spreadsheet)
json.job do
  json.id @inventory_spreadsheet.job.id
  json.job_id @inventory_spreadsheet.job.job_id
end