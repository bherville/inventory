json.generated_at @inventory_diagram.generated_at
json.generated_at_formatted @inventory_diagram.generated_at.nil? ? nil : @inventory_diagram.generated_at.to_formatted_s(:long_ordinal)
json.diagram_file_name @inventory_diagram.diagram_file_name
json.diagram_file_name_without_extension @inventory_diagram.diagram_file_name_without_extension
json.diagram_file_size @inventory_diagram.diagram_file_size
json.diagram_url @inventory_diagram.diagram.url
json.inventory_file_url @inventory_diagram.diagram.url
json.inventory_api_path api_inventory_diagram_path(@inventory_diagram)
json.inventory_path inventory_diagram_path(@inventory_diagram)
json.inventory_id @inventory_diagram.id
json.inventory_file_name @inventory_diagram.diagram_file_name_without_extension
json.user user_string(@inventory_diagram)
json.job do
  json.partial! 'api/v1/jobs/job', job: @inventory_diagram.job
end