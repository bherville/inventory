json.generated_at @inventory_diagram.generated_at
json.generated_at_formatted @inventory_diagram.generated_at.nil? ? nil : @inventory_diagram.generated_at.to_formatted_s(:long_ordinal)
json.inventory_api_path api_inventory_diagram_path(@inventory_diagram)
json.inventory_path inventory_diagram_path(@inventory_diagram)
json.inventory_id @inventory_diagram.id
json.user user_string(@inventory_diagram)
json.job do
  json.id @inventory_diagram.job.id
  json.job_id @inventory_diagram.job.job_id
end