json.id job.id
json.job_id job.job_id
json.resource_id job.statusable_id
json.resource_type job.statusable_type
json.resource_type_human job.statusable_type.to_s.underscore.humanize.split.map(&:capitalize).join(' ')
json.status job.status
json.completed_at job.completed_at
json.session_id job.session_id
json.api_path url_for(:controller => job.statusable_type.to_s.pluralize.underscore, :action => :show, :id => job.statusable_id, :only_path => true) if job.statusable_id && job.statusable_type
json.path url_for(:controller => "/#{job.statusable_type.to_s.pluralize.underscore}", :action => :show, :id => job.statusable_id, :only_path => true) if job.statusable_id && job.statusable_type