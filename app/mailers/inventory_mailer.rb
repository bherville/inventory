class InventoryMailer < ActionMailer::Base
  default from: APP_CONFIG['smtp']['from_address']

  def send_inventory(email_address, resource_id, context)
    @resource       = context.find(resource_id)
    @inventory      = @resource.inventory
    @inventory_url  = url_for(@resource)

    attachments[@resource.to_s] = File.read(@resource.inventory.path)

    mail(to: email_address, subject: base_subject(context))
  end

  def send_inventory_filtered(email_address, resource_id, context, filter_params)
    @resource       = context.find(resource_id)
    @inventory      = @resource.inventory
    @inventory_url  = url_for(@resource)
    @filter_params  = filter_params

    attachments[@resource.to_s] = File.read(@resource.inventory.path)

    if @resource.is_a?(InventoryDiagram)
      subject = "#{base_subject(context)} Filtered - #{@resource.filter_params_title}"
    else
      subject = "#{base_subject(context)} Filtered"
    end

    mail(to: email_address, subject: subject)
  end

  private
  def base_subject(context)
    "#{t('inventory.inventory')} - #{context.to_s.underscore.split('_')[1].capitalize}"
  end
end
