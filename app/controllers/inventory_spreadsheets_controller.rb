class InventorySpreadsheetsController < ApplicationController
  load_and_authorize_resource

  # GET /inventory_spreadsheets
  def index
    @inventory_spreadsheets = InventorySpreadsheet.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /inventory_spreadsheets/1
  def show
    @inventory_spreadsheet = InventorySpreadsheet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /inventory_spreadsheets/generate
  def generate
    servers = Server.find_by_environment_attributes(params)

    attr = {
        :servers              => servers,
        :application_versions => (servers.map { |s| s.application_versions }).flatten.uniq,
        :server_databases     => (servers.map { |s| s.server_databases }).flatten.uniq,
        :server_relationships => (servers.map { |s| s.server_relationships }).flatten.uniq,
        :filter_params        => params,
        :user                 => current_user
    }

    @inventory_spreadsheet = InventorySpreadsheet.create_and_generate_async attr, params[:save_formats].to_sym, { :session_id => session[:session_id], :user_id => current_user.id }

    respond_to do |format|
      format.js
      format.html { redirect_to :back }
    end
  end

  # GET /inventory_spreadsheets/1/send_email
  def new_email
    @inventory_spreadsheet = InventorySpreadsheet.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def send_via_email
    @inventory_spreadsheet = InventorySpreadsheet.find(params[:id])

    @inventory_spreadsheet.send_email_delayed(params[:email_addresses].split("\n"))

    respond_to do |format|
      format.html { redirect_to inventory_spreadsheets_path }
    end
  end
end
