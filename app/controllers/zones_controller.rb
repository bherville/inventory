class ZonesController < ApplicationController
  load_and_authorize_resource

  # GET /zones
  def index
    @zones = Zone.order(:name)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /zones/1
  def show
    @zone = Zone.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
