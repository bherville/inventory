class ServerDatabasesController < ApplicationController
  load_and_authorize_resource

  before_filter :set_server_database, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @server_databases = ServerDatabase.all
    respond_with(@server_databases)
  end

  def show
    respond_with(@server_database)
  end

  def new
    @server_database = ServerDatabase.new
    respond_with(@server_database)
  end

  def edit
  end

  def create
    @server_database = ServerDatabase.new(params[:server_database])
    @server_database.save
    respond_with(@server_database)
  end

  def update
    @server_database.update_attributes(params[:server_database])
    respond_with(@server_database)
  end

  def destroy
    @server_database.destroy
    respond_with(@server_database)
  end

  private
    def set_server_database
      @server_database = ServerDatabase.find(params[:id])
    end
end
