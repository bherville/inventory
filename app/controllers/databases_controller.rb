class DatabasesController < ApplicationController
  load_and_authorize_resource

  before_filter :set_database, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @databases = Database.all
    respond_with(@databases)
  end

  def show
    respond_with(@database)
  end

  def new
    @database = Database.new
    respond_with(@database)
  end

  def edit
  end

  def create
    @database = Database.new(params[:database])
    @database.save
    respond_with(@database)
  end

  def update
    @database.update_attributes(params[:database])
    respond_with(@database)
  end

  def destroy
    @database.destroy
    respond_with(@database)
  end

  private
    def set_database
      @database = Database.find(params[:id])
    end
end
