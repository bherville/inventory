class AuditsController < ApplicationController
  authorize_resource :class => false

  def index
    @audits = Audited.audit_class.all
  end
end