class ServerRelationshipsController < ApplicationController
  load_and_authorize_resource

  before_filter :set_server_relationship, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @server_relationships = ServerRelationship.all
    respond_with(@server_relationships)
  end

  def show
    respond_with(@server_relationship)
  end

  def new
    @server_relationship = ServerRelationship.new
    respond_with(@server_relationship)
  end

  def edit
  end

  def create
    @server_relationship = ServerRelationship.new(params[:server_relationship])
    @server_relationship.save
    respond_with(@server_relationship)
  end

  def update
    @server_relationship.update_attributes(params[:server_relationship])
    respond_with(@server_relationship)
  end

  def destroy
    @server_relationship.destroy
    respond_with(@server_relationship)
  end

  private
    def set_server_relationship
      @server_relationship = ServerRelationship.find(params[:id])
    end
end
