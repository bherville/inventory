class Admin::AdminController < Admin::BaseController
  load_and_authorize_resource

  def index
    @admin_pages = Admin::ADMIN_PAGES

    respond_to do |format|
      format.html
    end
  end
end