class Admin::JobsController < Admin::BaseController
  load_and_authorize_resource

  # GET /jobs
  def index
    @jobs = Job.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /jobs/1
  def show
    @job = Job.find_by_id(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
