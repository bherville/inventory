class JobsController < ApplicationController
  load_and_authorize_resource

  # GET /jobs
  def index
    @jobs = Job.find_all_by_user_id(current_user.id)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /jobs/1
  def show
    @job = Job.find_by_id_and_user_id(params[:id], current_user.id)

    respond_to do |format|
      format.html
    end
  end
end
