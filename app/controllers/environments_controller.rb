class EnvironmentsController < ApplicationController
  load_and_authorize_resource

  # GET /environments
  def index
    @environments = Environment.order(:name)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /environments/1
  def show
    @environment = Environment.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
