class ServerApplicationsController < ApplicationController
  load_and_authorize_resource

  # GET /server_applications
  def index
    @server_applications = ServerApplication.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /server_applications/1
  def show
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end
end
