class API::VersionsController < ApplicationController
  def index
    versions = ApiConstraints.versions

    api_versions = {
        :versions => versions,
        :default  => versions.last.to_sym
    }

    respond_to do |format|
      format.json { render json: api_versions }
    end
  end
end
