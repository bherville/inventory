class API::V1::ServersController < ApplicationController
  # GET /servers.json
  def index
    @servers = Server.all

    respond_to do |format|
      format.json { render json: @servers }
      format.csv { send_data Server.generate_csv(@servers) }
    end
  end

  # GET /servers/1.json
  def show
    @server = Server.find(params[:id])

    respond_to do |format|
      format.json { render json: @server }
    end
  end
end
