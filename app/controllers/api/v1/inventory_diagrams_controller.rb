class API::V1::InventoryDiagramsController < ApplicationController
  # GET /inventory_diagrams.json
  def index
    @inventory_diagrams = InventoryDiagram.all

    respond_to do |format|
      format.json { render json: @inventory_diagrams }
    end
  end

  # GET /inventory_diagrams/1.json
  def show
    @inventory_diagram = InventoryDiagram.find(params[:id])

    respond_to do |format|
      format.json
    end
  end

  def send_email
    @inventory_diagram = InventoryDiagram.find(params[:id])

    @inventory_diagram.send_email(params[:email_addresses].split(','))

    respond_to do |format|
      format.json { render json: {:info => 'sent'}.to_json }
    end
  end
end
