class API::V1::ApplicationFamilyApplicationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :exists]

  # GET /application_family_applications.json
  def index
    @application_family_applications = ApplicationFamilyApplication.all

    respond_to do |format|
      format.json
    end
  end

  # GET /application_family_applications/1.json
  def show
    @application_family_application = ApplicationFamilyApplication.find(params[:id])

    respond_to do |format|
      format.json
    end
  end

  # POST /application_family_applications.json
  def create
    @application_family_application = ApplicationFamilyApplication.new(params[:application_family_application])

    respond_to do |format|
      if @application_family_application.save
        format.json { render json: @application_family_application, status: :created, location: @application_family_application }
      else
        format.json { render json: @application_family_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /application_family_applications/1.json
  def update
    @application_family_application = ApplicationFamilyApplication.find(params[:id])

    respond_to do |format|
      if @application_family_application.update_attributes(params[:application_family_application])
        format.json { head :no_content }
      else
        format.json { render json: @application_family_application.errors, status: :unprocessable_entity }
      end
    end
  end

  def exists
    application_family = ApplicationFamily.find_by_id(params[:application_family_id])
    application_family ||= ApplicationFamily.find_by_name(params[:application_family_name].downcase)

    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])

    application_family_application = ApplicationFamilyApplication.find_by_application_family_id_and_application_id application_family.id, application.id
    application_family_application.update_last_checked if application_family_application

    respond_to do |format|
      format.json { render json: application_family_application }
    end
  end
end
