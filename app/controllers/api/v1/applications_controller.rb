class API::V1::ApplicationsController < ApplicationController
  # GET /applications.json
  def index
    @applications = Application.all

    respond_to do |format|
      format.json { render json: @applications }
      format.csv { send_data Application.generate_csv(@applications) }
    end
  end

  # GET /applications/1.json
  def show
    @application = Application.find(params[:id])

    respond_to do |format|
      format.json { render json: @application }
    end
  end
end
