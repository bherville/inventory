class API::V1::ApplicationFamiliesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /application_families.json
  def index
    @application_families = ApplicationFamily.all

    respond_to do |format|
      format.json { render json: @application_families }
    end
  end

  # GET /application_families/1.json
  def show
    @application_family = ApplicationFamily.find(params[:id])

    respond_to do |format|
      format.json { render json: @application_family }
    end
  end

  # POST /application_families.json
  def create
    @application_family = ApplicationFamily.new(params[:application_families])

    respond_to do |format|
      if @application_family.save
        format.json { render json: @application_family, status: :created, location: @application_family }
      else
        format.json { render json: @application_family.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /application_families/1.json
  def update
    @application_family = ApplicationFamily.find(params[:id])

    respond_to do |format|
      if @application_family.update_attributes(params[:application_families])
        format.json { head :no_content }
      else
        format.json { render json: @application_family.errors, status: :unprocessable_entity }
      end
    end
  end
end
