class API::V1::InventorySpreadsheetsController < ApplicationController
  # GET /inventory_spreadsheets.json
  def index
    @inventory_spreadsheets = InventorySpreadsheet.all

    respond_to do |format|
      format.json { render json: @inventory_spreadsheets }
    end
  end

  # GET /inventory_spreadsheets/1.json
  def show
    @inventory_spreadsheet = InventorySpreadsheet.find(params[:id])

    respond_to do |format|
      format.json
    end
  end

  def send_email
    @inventory_spreadsheet = InventorySpreadsheet.find(params[:id])

    @inventory_spreadsheet.send_email(params[:email_addresses].split(','))

    respond_to do |format|
      format.json { render json: {:info => 'sent'}.to_json }
    end
  end
end
