class API::V1::JobsController < ApplicationController
  # GET /api/jobs
  def index
    if params[:filter_by]
      case params[:filter_by]
        when 'user'
          @jobs = Job.find_all_by_user_id(current_user.id)
        when 'user:complete_count'
          @jobs = Job.find_all_by_user_id_and_status(current_user.id, 'complete')

          jobs_hash = { :jobs => { :complete => @jobs.count } }
        when 'user:working_and_queued_count'
          @working_jobs = Job.find_all_by_user_id_and_status(current_user.id, 'working')
          @queued_jobs = Job.find_all_by_user_id_and_status(current_user.id, 'queued')

          jobs_hash = { :jobs => { :queued => @queued_jobs.count, :working => @working_jobs.count } }
        when 'session'
          @jobs = Job.find_all_by_session_id(session[:session_id])
        when 'session:complete_count'
          @jobs = Job.find_all_by_session_id_and_status(session[:session_id], 'complete')

          jobs_hash = { :jobs => { :complete => @jobs.count } }
        when 'session:working_and_queued_count'
          @working_jobs = Job.find_all_by_session_id_and_status(session[:session_id], 'working')
          @queued_jobs = Job.find_all_by_session_id_and_status(session[:session_id], 'queued')

          jobs_hash = { :jobs => { :queued => @queued_jobs.count, :working => @working_jobs.count } }
      end
    else
      @jobs = Job.all

      respond_to do |format|
        format.json
      end
    end

    if jobs_hash
      respond_to do |format|
        format.json { render :json => jobs_hash }
      end
      else
        respond_to do |format|
          format.json
        end
    end
  end

  # GET /api/jobs/1.json
  def show
    @job = Job.find(params[:id])

    respond_to do |format|
      format.json
    end
  end
end
