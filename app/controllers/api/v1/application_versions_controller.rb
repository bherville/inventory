class API::V1::ApplicationVersionsController < ApplicationController
  # GET /application_versions.json
  # GET /application_versions.csv
  def index
    @application_versions = ApplicationVersion.all

    respond_to do |format|
      format.json { render json: @application_versions }
      format.csv { send_data ApplicationVersion.generate_csv(@application_versions) }
    end
  end

  # GET /application_versions/1.json
  def show
    @application_version = ApplicationVersion.find(params[:id])

    respond_to do |format|
      format.json { render json: @application_version }
    end
  end
end
