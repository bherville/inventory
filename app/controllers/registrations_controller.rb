class RegistrationsController < Devise::RegistrationsController
  def update
    @user = resource

    account_update_params = params[:user]
    account_update_params.delete('roles')

    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      account_update_params.delete('password')
      account_update_params.delete('password_confirmation')
    end

    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to :my_account
    else
      render 'edit'
    end
  end
end