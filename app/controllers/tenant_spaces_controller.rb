class TenantSpacesController < ApplicationController
  load_and_authorize_resource

  # GET /tenant_spaces
  def index
    @tenant_spaces = TenantSpace.order(:name)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /tenant_spaces/1
  def show
    @tenant_space = TenantSpace.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
