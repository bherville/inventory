class InventoryDiagramsController < ApplicationController
  load_and_authorize_resource

  # GET /inventory_diagrams
  def index
    @inventory_diagrams = InventoryDiagram.all


    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /inventory_diagrams/1
  def show
    @inventory_diagram = InventoryDiagram.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /inventory_diagrams/generate
  def generate
    servers = Server.find_by_environment_attributes(params)

    # InventoryDiagram#filter_params_title expects application_family to be an array
    params['application_family'] = params['application_family'] ? [params['application_family']] : []

    attr = {
          :servers        => servers,
          :filter_params  => params,
          :user           => current_user
    }

    @inventory_diagram = InventoryDiagram.create_and_generate_async attr, params[:save_formats].to_sym, { :session_id => session[:session_id], :user_id => current_user.id }

    respond_to do |format|
      format.js
      format.html { redirect_to :back }
    end
  end

  # GET /inventory_diagrams/1/send_email
  def new_email
    @inventory_diagram = InventoryDiagram.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def send_via_email
    @inventory_diagram = InventoryDiagram.find(params[:id])

    if @inventory_diagram.filter_params
      @inventory_diagram.send_email_delayed_filtered(params[:email_addresses].split("\n"), @inventory_diagram.filter_params)
    else
      @inventory_diagram.send_email_delayed(params[:email_addresses].split("\n"))
    end

    respond_to do |format|
      format.html { redirect_to inventory_diagrams_path }
    end
  end
end
