class DataCentersController < ApplicationController
  load_and_authorize_resource

  # GET /data_centers
  def index
    @data_centers = DataCenter.order(:name)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /data_centers/1
  def show
    @data_center = DataCenter.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
