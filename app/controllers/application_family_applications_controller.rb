class ApplicationFamilyApplicationsController < ApplicationController
  load_and_authorize_resource

  # GET /application_family_applications
  def index
    @application_family_applications = ApplicationFamilyApplication.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /application_family_applications/1
  def show
    @application_family_application = ApplicationFamilyApplication.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /application_family_applications/new
  def new
    @application_family_application = ApplicationFamilyApplication.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /application_family_applications/1/edit
  def edit
    @application_family_application = ApplicationFamilyApplication.find(params[:id])
  end

  # POST /application_family_applications
  def create
    @application_family_application = ApplicationFamilyApplication.new(params[:application_family_application])

    respond_to do |format|
      if @application_family_application.save
        format.html { redirect_to @application_family_application, notice: 'Application family application was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /application_family_applications/1
  def update
    @application_family_application = ApplicationFamilyApplication.find(params[:id])

    respond_to do |format|
      if @application_family_application.update_attributes(params[:application_family_application])
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /application_family_applications/1
  def destroy
    @application_family_application = ApplicationFamilyApplication.find(params[:id])
    @application_family_application.destroy

    respond_to do |format|
      format.html { redirect_to application_family_applications_url }
    end
  end
end
