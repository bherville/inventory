class ApplicationFamiliesController < ApplicationController
  load_and_authorize_resource

  # GET /application_families
  def index
    @application_families = ApplicationFamily.all

    respond_to do |format|
      format.html # index.html.erb
      format.csv  { render csv: @application_families }
    end
  end

  # GET /application_families/1
  def show
    @application_family = ApplicationFamily.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /application_families/new
  def new
    @application_family = ApplicationFamily.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /application_families/1/edit
  def edit
    @application_family = ApplicationFamily.find(params[:id])
  end

  # POST /application_families
  def create
    @application_family = ApplicationFamily.new(params[:application_family])

    respond_to do |format|
      if @application_family.save
        format.html { redirect_to @application_family, notice: 'Application family was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /application_families/1
  def update
    @application_family = ApplicationFamily.find(params[:id])

    respond_to do |format|
      if @application_family.update_attributes(params[:application_families])
        format.html { redirect_to @application_family, notice: 'Application family was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /application_families/1
  def destroy
    @application_family = ApplicationFamily.find(params[:id])
    @application_family.destroy

    respond_to do |format|
      format.html { redirect_to application_families_url }
    end
  end
end
